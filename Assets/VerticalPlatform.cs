﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalPlatform : MonoBehaviour
{
    private PlatformEffector2D effector;
    public float waitTimeSet;
    private float waitTime;
    public float timerBackSet;
    private float timerBack;


    private bool IsChanged = false;
    // Start is called before the first frame update
    void Start()
    {
        waitTime=waitTimeSet;
        timerBack=timerBackSet;
        effector = GetComponent<PlatformEffector2D>();
    }

    // Update is called once per frame
    
    private void OnCollisionStay2D(Collision2D other) {

        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            waitTime=waitTimeSet;
        }
        

        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (waitTime>= waitTimeSet)
            {
                IsChanged=true;
                effector.rotationalOffset = 180;
                waitTime = waitTimeSet;
            }
            else
            {
                waitTime += Time.deltaTime;
            }
        }
    }

    private void FixedUpdate() {
        if (IsChanged==true)
        {
            if (timerBack>= timerBackSet)
            {
                effector.rotationalOffset = 0;
                timerBack=0f;
                IsChanged=false;
                waitTime=0;
            }
            else
            {
                timerBack += Time.deltaTime;
            }
        }
        
    }

}
