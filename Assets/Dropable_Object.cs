﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Dropable_Object : MonoBehaviour
{
    private Rigidbody2D rb; 
    private Animator animator;
    private bool isDropping=false;
    private bool shakeEnd=false;
    private bool oneTimeShake=false;
    [SerializeField]
    private float speed = 5.0f;
    [SerializeField]
    private float amount = 1.0f;
    private Vector2 vector;
    public float droptimeSet;
    private float droptime=0f;
    [SerializeField]
    private GameObject splashParticle;
    private float Xoffset;
    private float Yoffset;
    
    Vector2 startingPos;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        startingPos.x = transform.position.x;
        startingPos.y = transform.position.y;
        Xoffset = .15f;
        Yoffset = .15f;
    }

    // Update is called once per frame
    void Update()
    {
        Timer();
        if (isDropping)
        { 
            while (rb.gravityScale<=1)
            {
                rb.gravityScale += 0.01f;
            }
        }
    }

    private void OnCollisionStay2D(Collision2D other) {
        if (other.gameObject.CompareTag("Player") && !oneTimeShake)
        {
            oneTimeShake=true;
            
            droptime = 0;

        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.transform.tag=="WaterTopSide")
        {
            Vector2 spawnPos = new Vector2(this.transform.position.x + Xoffset, transform.position.y + Yoffset);
            Instantiate(splashParticle,spawnPos, Quaternion.Euler(0.0f,0.0f,0.0f));
            
        }
        if (other.transform.tag=="WaterSide")
        {
            Destroy(gameObject);
        }
    }

    private void Timer()
    {
        if (oneTimeShake)
        {
            if (droptime>=droptimeSet)
            {
                Drop();
            }
            else
            {
                transform.position = new Vector3(startingPos.x + Mathf.Sin(Time.time * speed) * amount ,startingPos.y + Mathf.Sin(Time.time * speed) * amount,transform.position.z);
                droptime+=Time.deltaTime;
            }
        }
    }

    public void Drop()
    {
        rb.bodyType= RigidbodyType2D.Dynamic;
        rb.gravityScale=0f;
        isDropping=true; 
    }
}
