﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicProjectile : MonoBehaviour
{
    private float speed;
    private bool start = false;
    private Vector2 playerBottomPoint;
    private Vector2 destinationPoint;
    public float destinationPointOffset = 0.05f;
    private bool getPosition = false;
    public D_MagicAttackState magicstatedata;
    public Transform childObject;
    private Animator animator;
    private bool playAnimationBool = false;
    public float groundCheckDistance;
    public LayerMask whatIsGround;

    void Start()
    {
        animator = childObject.GetComponent<Animator>();
    }
    
    void Update()
    {
        
        
    }
    private void FixedUpdate() {

        if (playAnimationBool)
        {
            animator.SetTrigger("Explode");            
        }
        if (start)
        {
            if (!getPosition)
            {
                playerBottomPoint.x = GameObject.FindGameObjectWithTag("Player").transform.position.x;
                playerBottomPoint.y = GameObject.FindGameObjectWithTag("Player").transform.position.y;
                
                getPosition = true;
            }
            RaycastHit2D hit = Physics2D.Raycast(playerBottomPoint, Vector2.down, groundCheckDistance, whatIsGround);
            if (hit)
            {
                destinationPoint = hit.point; 
                destinationPoint = new Vector2(destinationPoint.x,destinationPoint.y - destinationPointOffset);
                this.transform.position = Vector2.MoveTowards(this.transform.position, destinationPoint, speed * Time.deltaTime);
            }                       
        }

        
    }

    public void StartMoving(float projectileSpeed)
    {
        this.speed = projectileSpeed;
        start = true;
    }

    public void playAnimation()
    {
        playAnimationBool=true;
    }

   public void OnDrawGizmos()
   {
        Gizmos.DrawWireSphere(childObject.position, magicstatedata.attackRadius);
   }

   public void EndofAnimationToChildObject()
   {
       Destroy(this.gameObject);
   }
   
}
