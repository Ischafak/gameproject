﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPointHolder : MonoBehaviour
{
    public Transform weaponPointSword;
    public Transform weaponPointSpear;
    public Transform bowPoint;

}
