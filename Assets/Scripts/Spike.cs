﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
    public float knockbackdur;
    private bool canKnockback=true;
    private float timer;
    private Transform mainObject1;
    List<Transform> mainObject2 = new List<Transform>();
    private bool isTouching = false;
    private bool damageToPlayer = false;
    private bool damageToEnemy = false;
    protected AttackDetails attackDetails;
    public float attackDamage = 10.0f;

    void Start()
    {
        timer=0;
        attackDetails.damageAmount = attackDamage;
        attackDetails.position = this.transform.position;
        attackDetails.DamageType = true;
    }
    private void Update() 
    {    
        
        if (canKnockback==false)
        {
            timer+=Time.deltaTime;
            if (timer>=knockbackdur)
            {
                canKnockback=true;

            }
        }
        if (canKnockback && isTouching)
        {
            if (damageToPlayer)
            {
                mainObject1.SendMessage("Damage", attackDetails);
            }
            if (damageToEnemy)
            {
                foreach (Transform item in mainObject2)
                {
                    item.SendMessage("Damage", attackDetails);
                }
                
            }
            canKnockback=false;
            timer=0;
        }
        
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.CompareTag("Player"))
        {
            mainObject1 = other.collider.transform;
            isTouching = true; 
            damageToPlayer = true;
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            mainObject2.Add(other.collider.transform.parent);
            isTouching = true; 
            damageToEnemy = true;
        }
              
    }

    private void OnCollisionExit2D(Collision2D other) {
        if (other.gameObject.CompareTag("Player"))
        {
            isTouching = false;
            damageToPlayer = false;
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            mainObject2.Remove(other.collider.transform.parent);
            if (mainObject2.Count==0)
            {
                damageToEnemy = false;
                isTouching = false;
            }
            
        }
        
    }
}
 