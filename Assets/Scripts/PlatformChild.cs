﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformChild : MonoBehaviour
{
    // Start is called before the first frame update

    private float speed;
    void Start()
    {
        
    }

    // Update is called once per frame

    private void FixedUpdate() {
        speed = Mathf.Abs(Input.GetAxisRaw("Horizontal"));
        
        
    }

     void OnCollisionEnter2D(Collision2D other){
     if (other.gameObject.CompareTag("Player")) {
         other.transform.parent = transform;
     }
 }
 
  
 void OnCollisionExit2D(Collision2D other){
     if (other.gameObject.CompareTag("Player")) {
         other.transform.parent = null;
     }
      }

     private void OnCollisionStay2D(Collision2D other) {
         if (other.gameObject.CompareTag("Player")) {

             if (speed!=0)
             {
                 other.transform.parent = null;
             }
             if (speed==0)
             {
                 other.transform.parent = transform;
             }
         
        }  
        
         
     }

}
