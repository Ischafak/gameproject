﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Image hpEffectImage;
    public Image hpImage;

    private float hp;
    [SerializeField] private float maxHp;
    [SerializeField] private float hurtSpeed;
    [SerializeField] private float waitBeforeActionDuration = .5f;
    private float waitBeforeActionStartTime;

    public void SetMaxHealth(float health)
    {
        maxHp = health;
        hp = maxHp;
    }
    public void SetHealth(float health)
    {
        hp = health;
        waitBeforeActionStartTime = Time.time;
    }
    private void Update() {
        hpImage.fillAmount = hp / maxHp;
        if (Time.time >= waitBeforeActionStartTime + waitBeforeActionDuration)
        {
            if (hpEffectImage.fillAmount > hpImage.fillAmount)
            {   
                hpEffectImage.fillAmount -= hurtSpeed;
            }
            else
            {
                hpEffectImage.fillAmount = hpImage.fillAmount;
            }
        }
        
    }
   
}
