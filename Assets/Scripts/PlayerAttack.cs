﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{   
    [SerializeField]
    private Transform attack1HitBoxPos;
    [SerializeField]
    private LayerMask whatIsDamageable;
    [SerializeField]
    private LayerMask whatIsGround;
    [SerializeField]
    private float attack1Radius, attack1Damage, critDamage;
    [SerializeField]
    private Transform bodyCheck;
    [SerializeField]
    private float characterRadius;
    [SerializeField]
    private LayerMask damagableMask;
    [SerializeField]
    private float bodyDamage = 10; 
    private bool bowReloadTimeOnce=false;
    private bool moveblocked = false;
    private int playerFacingDirection;
    private AttackDetails attackDetails;

    private float attackTimer2;
    public float attackTimer2Set=0.5f;
    private float timeBtwAttack;
    public float timeBtwAttackSet;

    private float BowtimeBtwAttack;
    public float BowtimeBtwAttackSet;


    private float BowReloadTime;
    public float BowReloadTimeSet = 0.20f;
    [SerializeField]
    private float stunDamageAmount = 1f;

    [SerializeField]
    private float damageMoveValue = 20f;
    
    [SerializeField]
    private Transform ledgeCheck;
    [SerializeField]
    private float ledgeCheckDistance;
    [SerializeField]
    private float shieldHopValue;
    [SerializeField]
    private float shieldWaitDuration;
    private float shieldWaitStart;
    
    public List<GameObject> actors = new List<GameObject>();
    private bool BowReady=false;



    private Rigidbody2D rb;

    public bool isAttacking = true;
    private bool canCombo = false;
    private int attackCount;
    public bool attackAnimEnd;
    private bool canAttack;
    private Animator anim;
    private PlayerControllerX playerController;
    private bool materialChanged = false;

    private bool isBowAttacking = false;
    private PlayerStats PS;
    public Transform firePoint;
    public GameObject arrowPrefab;
    private int typeOfMeleeWeapon;
    private int typeOfRangedWeapon;
    private GameObject bow;
    [SerializeField]
    private Transform[] meleeWeaponHitPositions;
    
    void Start()
    {
        typeOfMeleeWeapon = 0;
        typeOfRangedWeapon = 0;
        canAttack = false;
        isAttacking = false;
        BowtimeBtwAttack = 0f;
        rb= GetComponent<Rigidbody2D>();
        playerController=GetComponent<PlayerControllerX>();
        anim = GetComponent<Animator>();
        attackCount=0;
        PS = GetComponent<PlayerStats>();
        attackAnimEnd = true;
        
    }

    // Update is called once per frame
    void Update()
    {
        
        Collider2D[] detectedObjects = Physics2D.OverlapCircleAll(bodyCheck.position, characterRadius, damagableMask);
        if (detectedObjects.Length>0)
        {
            foreach (Collider2D collider in detectedObjects)
            {
                playerController.movementSpeed = .7f;
                materialChanged = true;
            }
        }
        else
        {
            if (materialChanged)
            {
                playerController.movementSpeed = 1.7f;
                materialChanged = false;
            }
        }
        
       if (Input.GetButtonDown("Fire2") && BowtimeBtwAttack<0.1 && playerController.isGrounded && typeOfRangedWeapon!=0 && !isAttacking)
       {
           rb.velocity = new Vector2(0.0f, 0.0f);
           isBowAttacking=true;
           bowReloadTimeOnce=false;
           playerController.canMove=false;
           playerController.canFlip=false;
           
           if (bow)
           {
               bow.GetComponent<Animator>().SetTrigger("fire");
           }
           
       }
       if (Input.GetButtonDown("Fire2") && BowtimeBtwAttack<0.1 && actors.Count>0 && !playerController.isGrounded && !isAttacking)
       {   
           isBowAttacking=true;
           bowReloadTimeOnce=false;
           playerController.canMove=false;
           playerController.canFlip=false;
       }

        anim.SetInteger("AttackCount",attackCount);
        anim.SetBool("IsAttacking",isAttacking);
        anim.SetBool("CanCombo",canCombo);
        anim.SetBool("BowAttack",isBowAttacking);

        if (Input.GetMouseButtonDown(0) && timeBtwAttack<0.1 && playerController.isGrounded &&  typeOfMeleeWeapon!=0 && !isBowAttacking && Time.time >= shieldWaitStart + shieldWaitDuration)
        {
            canAttack = true;
        }
        if (canAttack && attackAnimEnd)
        {
            if (isAttacking == false)
           {
                canAttack = false;
                attackAnimEnd = false;
                moveblocked=false;
                rb.velocity = new Vector2(0.0f, 0.0f);
                attackTimer2 = attackTimer2Set;
                playerFacingDirection = playerController.GetFacingDirection();
           bool checkLedge = Physics2D.Raycast(ledgeCheck.position, Vector2.down, ledgeCheckDistance, whatIsGround);
           if (checkLedge)
           {
                Vector2 forceToAdd = new Vector2(damageMoveValue * playerFacingDirection, rb.velocity.y);
                rb.AddForce(forceToAdd, ForceMode2D.Force);
           }
           
            if (attackCount<3)
            {
                attackCount++;
            }
            else
            {
                attackCount=1;  
            }
               isAttacking = true;
           }
           
           timeBtwAttack=timeBtwAttackSet;
           playerController.canMove=false;
           playerController.canFlip=false;
        }
        if (timeBtwAttack>0)
        {
            timeBtwAttack-=Time.deltaTime;
        }
        CheckAttackEndTimer();
    }
    
    private void CheckAttackEndTimer()
    {
        if (attackTimer2>0)
        {
            canCombo=true;
            if (Time.time >= shieldWaitStart + shieldWaitDuration)
            {
                attackTimer2-=Time.deltaTime;
            }
            
        }
        if (attackTimer2<=0.1)
        {
            canCombo=false;
            attackCount=0;
        }
        if (BowtimeBtwAttack>0)
        {
           
            BowtimeBtwAttack-=Time.deltaTime;
        }
        
        if (BowReloadTime>0)
        {
           BowReloadTime-=Time.deltaTime;
        }
        
    }

    private void Shoot()
    {   
        playerFacingDirection = playerController.GetFacingDirection();
        if (playerFacingDirection==1)
        {
            Instantiate(arrowPrefab,firePoint.position,Quaternion.Euler(0.0f,0.0f,-90.0f)); 
        }
        else
        {
            Instantiate(arrowPrefab,firePoint.position,Quaternion.Euler(0.0f,0.0f,90.0f));
        }
        
        isBowAttacking=false;
    }
    public void BowAttackEnd()
    {
        playerController.canMove=true;
        playerController.canFlip=true;
        isBowAttacking=false;
    }
    private void CheckAttackHitBox()
    {
        Collider2D[] detectedObjects = Physics2D.OverlapCircleAll(attack1HitBoxPos.position, attack1Radius, whatIsDamageable);
        
        attackDetails.position = transform.position;
        attackDetails.stunDamageAmount = stunDamageAmount;
        if (attackCount==3)
        {
            attackDetails.stunDamageAmount =2;
            attackDetails.crit=true;
            attackDetails.damageAmount = critDamage;
        }
        else
        {
            attackDetails.crit=false;
            attackDetails.damageAmount = attack1Damage;
            attackDetails.stunDamageAmount =1;

            if (typeOfMeleeWeapon==2)
            {
                if (attackCount==2)
                {
                    attackDetails.damageAmount = 20f;
                    attackDetails.crit=false;
                    attackDetails.DamageType = false;
                    attackDetails.stunDamageAmount = 0f;
                    InvokeRepeating("Multihit", 0f, .1f);
                }
            }
        }
        foreach (Collider2D collider in detectedObjects)
        {
            collider.transform.parent.SendMessage("Damage",attackDetails);
        }
    }

    private void Multihit()
    {
        Collider2D[] detectedObjects = Physics2D.OverlapCircleAll(attack1HitBoxPos.position, attack1Radius, whatIsDamageable);
        foreach (Collider2D collider in detectedObjects)
        {
            
            collider.transform.parent.SendMessage("Damage",attackDetails);
        }
    }
    private void OnDrawGizmos() 
    {
        Gizmos.DrawWireSphere(bodyCheck.position, characterRadius);
        Gizmos.DrawWireSphere(attack1HitBoxPos.position, attack1Radius);
        Gizmos.DrawLine(ledgeCheck.position, new Vector2(ledgeCheck.position.x, ledgeCheck.position.y - ledgeCheckDistance));
    }
    public void AttackEnd()
    {
        CancelInvoke("Multihit");
        isAttacking=false; 
         playerController.canMove=true;
            playerController.canFlip=true;
            moveblocked=true;
    }
    
    public void AttackAnimFinish()
    {
        attackAnimEnd = true;
    }

    private void Damage(AttackDetails attackDetails)
    {
        if (!playerController.GetSlideStatus())
        {
            int direction;

            PS.DecreaseHealth(attackDetails.damageAmount);
            if (attackDetails.position.x < transform.position.x)
            {
                direction=1;
            }
            else
            {
                direction=-1;
            }
            playerController.Knockback(direction);
        }
    }
    private void ShieldBlock()
    {
        if (attackCount!=2)
        {
        rb.velocity = new Vector2(0f, shieldHopValue);
        shieldWaitStart = Time.time;
        }
    }

     public void TypeOfMeleeWeapon(int value)
     {
         this.typeOfMeleeWeapon = value;
         anim.SetInteger("meleeWeaponType",typeOfMeleeWeapon);
         
        attack1HitBoxPos =  meleeWeaponHitPositions[value-1];
        if (value == 1)
        {
            attack1Radius = 0.2f;
        }
        else if (value == 2)
        {
            attack1Radius = 0.28f;
        }
         
     }

     public void TypeOfRangedWeapon(int value)
     {
         this.typeOfRangedWeapon = value;
         bow = GameObject.FindGameObjectWithTag("Ranged");
     }
}
