﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAssets : MonoBehaviour
{
    private void Start() {
        for (int i = 0; i < LootTable.Count; i++)
        {
            LootTable[i].item.GetComponent<Pickup>().weaponRareness = LootTable[i].dropRarity;
        }
    }
    private static GameAssets _i;

    public static GameAssets i 
    {
        get {
            if (_i == null)
            {
                _i = (Instantiate(Resources.Load("GameAssets")) as GameObject).GetComponent<GameAssets>();
            }
                return _i;
        }
    }
    public enum WeaponRareness {Common, Rare, Epic, Legendary};
    public Transform pfDamagePopup; 
    public PhysicsMaterial2D rbMaterialFriction;
    public PhysicsMaterial2D rbMaterialNormal;
    public GameObject stunSprite;
    [System.Serializable]
    public class DropCurrency
    {
        public string name;
        public GameObject item;
        public WeaponRareness dropRarity;
    }

    [System.Serializable]
    public class DropCurrencyValue
    {
        public WeaponRareness rareType;
        public int rareValue;
    }

    public List <DropCurrency> LootTable = new List<DropCurrency>();
    public List <DropCurrencyValue> rarenessValues = new List<DropCurrencyValue>();

    
}
