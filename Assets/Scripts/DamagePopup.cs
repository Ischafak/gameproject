﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamagePopup : MonoBehaviour
{   
    private Vector3 moveVector;
    private TextMeshPro textMesh;
    [SerializeField]
    private float disappearTimer;
    [SerializeField]
    private float disappearSpeed = 3f;
    private Color textColor;
    private const float disappear_Timer_Max = 1f;
    private float increaseScaleAmount = .3f;
    private float decreaseScaleAmount = .3f;
    private static int sortingorder;
    [SerializeField]
    private float moveForceXMax;
    [SerializeField]
    private float moveForceXMin;
    [SerializeField]
    private float moveForceYMax;
    [SerializeField]
    private float moveForceYMin;
    private float critForce;
    public static DamagePopup Create(Vector3 position, AttackDetails attackDetails, int lastAttackDirection)
    {
        Transform damagePopupTransform = Instantiate(GameAssets.i.pfDamagePopup, position, Quaternion.identity);
        DamagePopup damagePopup = damagePopupTransform.GetComponent<DamagePopup>();
        damagePopup.Setup(attackDetails, lastAttackDirection);

        return damagePopup;
    }
    
    private void Awake() {
        textMesh = transform.GetComponent<TextMeshPro>();
    }
    public void Setup(AttackDetails attackDetails, int lastAttackDirection)
    {
        textMesh.SetText(attackDetails.damageAmount.ToString());
        if (!attackDetails.crit)
        {
            critForce = 1.3f;
            textMesh.fontSize = 2f;
            textColor = new Color32(227,184,20,255);
        }
        else
        {
            critForce = 2f;
            textMesh.fontSize = 3f;
            textColor = new Color32(226,38,20,255);
        }
        textMesh.color = textColor;
        
        textColor = textMesh.color;
        disappearTimer = disappear_Timer_Max;

        sortingorder++;
        textMesh.sortingOrder = sortingorder;
        moveVector = new Vector3(Random.Range(moveForceXMin,moveForceXMax) * lastAttackDirection,Random.Range(moveForceXMin,moveForceXMax)) * critForce;
    }
    
    private void Update() {
        transform.position += moveVector * Time.deltaTime;
        moveVector -= moveVector * 8f * Time.deltaTime;

        if (disappearTimer > disappear_Timer_Max * .5f)
        {
            transform.localScale += Vector3.one * increaseScaleAmount * Time.deltaTime;
        }
        else
        {
            transform.localScale -= Vector3.one * decreaseScaleAmount * Time.deltaTime;
        }
        disappearTimer -= Time.deltaTime;
        if (disappearTimer < 0)
        {
            textColor.a -= disappearSpeed * Time.deltaTime;
            textMesh.color = textColor;
            if (textColor.a < 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
