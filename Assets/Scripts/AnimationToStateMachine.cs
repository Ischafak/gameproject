﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationToStateMachine : MonoBehaviour
{
    public AttackState attackState;
    private int colliderNumber = 0;
    private int moveNumber = 0;

    private int explodeNumber = 0;
   private void TriggerAttack()
   {
       attackState.TriggerAttack(colliderNumber);
       colliderNumber++;
   }

   private void FinishAttack()
   {
       attackState.Finishattack();
       colliderNumber = 0;
       moveNumber = 0;
       explodeNumber = 0;
   }
   
   private void FinishComboAttack()
   {
       attackState.FinishComboAttack();
       colliderNumber = 0;
   }

   private void SpawnProjectile()
   {
       attackState.SpawnProjectile();
   }

   private void MoveProjectile()
   {
       attackState.MoveProjectile(moveNumber);
       moveNumber++;
   }
   private void ExplodeProjectile()
   {
       attackState.ExplodeProjectile(explodeNumber);
       explodeNumber++;
   }
}
