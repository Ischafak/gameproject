﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    [SerializeField]
    private Transform spawnPoint;
    private Animator animator;
    private bool isChestOpen = false;
    [SerializeField]
    private float weaponSpeedX,weaponSpeedY;
    [SerializeField] GameObject canvas;

    private void Start() {
        animator = GetComponent<Animator>();
    }
    private void OpenChest()
    {
        int itemWeight = 0;

        for (int i = 0; i < GameAssets.i.LootTable.Count; i++)
        {
           GameAssets.WeaponRareness rareness = GameAssets.i.LootTable[i].dropRarity;
            for (int j = 0; j < GameAssets.i.rarenessValues.Count; j++)
            {
                if (GameAssets.i.rarenessValues[j].rareType==rareness)
                {
                     itemWeight+=GameAssets.i.rarenessValues[j].rareValue;
                     break;
                }
            }
        }
        int randomValue = Random.Range(0, itemWeight);
        for (int i = 0; i < GameAssets.i.LootTable.Count; i++)
        {
            GameAssets.WeaponRareness rareness = GameAssets.i.LootTable[i].dropRarity;
            for (int j = 0; j < GameAssets.i.rarenessValues.Count; j++)
            {
                if (GameAssets.i.rarenessValues[j].rareType==rareness)
                {  
                     if (randomValue <= GameAssets.i.rarenessValues[j].rareValue)
                     {
                        GameObject weapon = Instantiate(GameAssets.i.LootTable[i].item, spawnPoint.position, Quaternion.identity);
                        weapon.GetComponent<Rigidbody2D>().velocity = new Vector2(weaponSpeedX, weaponSpeedY);
                         
                         return;
                     }
                     randomValue-=GameAssets.i.rarenessValues[j].rareValue;
                }
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other) {
        if (!isChestOpen)
        {  
            if (other.CompareTag("Player"))
            {
                canvas.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    
                    animator.SetTrigger("OpenChest");
                    isChestOpen=true;
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        canvas.SetActive(false);
    }

    public void OpenChestTriggerTime()
    {
        OpenChest();
    }
}
