﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialTintColor : MonoBehaviour
{
    private Material material;
    private Color materialTintColor;
    private float tintFadeSpeed;
    private bool isChanged = false;

    private void Awake() {
        materialTintColor = new Color(1,0,0,0);
        SetMaterial(this.gameObject.GetComponent<SpriteRenderer>().material);
        tintFadeSpeed = 6f;
    }
    private void Update() {

        if (isChanged)
        {
            if (materialTintColor.a > 0)
            {
                materialTintColor.a = Mathf.Clamp01(materialTintColor.a - tintFadeSpeed * Time.deltaTime);
                material.SetColor("_Tint",materialTintColor);
            }
            else{
                isChanged=false;
            }
        }
    }
    public void SetMaterial(Material material)
    {
        this.material = material;
    }
    public void SetTintColor(Color color, bool changeMode)
    {
        materialTintColor = color;
        material.SetColor("_Tint", materialTintColor);
        if (changeMode)
        {
            isChanged=true;
        }
        
    }
    public void SetTintFadeSpeed(float tintFadeSpeed)
    {
        this.tintFadeSpeed = tintFadeSpeed;
    }

}
