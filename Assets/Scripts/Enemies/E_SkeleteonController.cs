﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E_SkeleteonController : MonoBehaviour
{
   private enum State 
   {
       Moving,
       Knockback,
       Dead
   }

   private State curentState;

   [SerializeField]
   private float groundCheckDistance, wallCheckDistance, movementSpeed, maxHealth, KnockbackDuration,
                touchDamageCooldown, touchDamage, touchDamageWidth, touchDamageHeght;

   [SerializeField]
   private Transform groundCheck, WallCheck, touchDamageCheck;

   [SerializeField]
   private LayerMask whatisGround, whatisPlayer;

   [SerializeField]
   private Vector2 KnockbackSpeed;

   [SerializeField]
   private GameObject hitParticle, deathChunkParticle, deathBloodParticle;
   private float currentHealth, KnockbackStartTime, lastTouchDamageTime;
   private float[] attackDetails = new float[2];
   private bool groundDetected, wallDetected;
   private int facingDirection, damageDirection;
   private Vector2 movement, touchDamageBotLeft, touchDamageTopRight;
   private GameObject alive;
   private Rigidbody2D aliveRb;
   private Animator aliveAnim;

   private void Start() 
   {
       alive = transform.Find("Alive").gameObject;
       aliveRb = alive.GetComponent<Rigidbody2D>();
       aliveAnim = alive.GetComponent<Animator>();

       facingDirection = 1;
       currentHealth = maxHealth;
   }

   private void Update() 
   {
       switch(curentState)
       {
            case State.Moving:
                UpdateMovingState();
                break;
            case State.Knockback:
                UpdateKnockbackState();
                break;
            case State.Dead:
                UpdateDeadState();
                break;

       }
   }

   //-WALKING STATE-------------------------------------------------------

   private void EnterMovingState()
   {

   }

   private void UpdateMovingState()
   {
       groundDetected = Physics2D.Raycast(groundCheck.position, Vector2.down, groundCheckDistance, whatisGround);
       wallDetected = Physics2D.Raycast(WallCheck.position, Vector2.right, wallCheckDistance, whatisGround);

       CheckTouchDammage();

       if (!groundDetected || wallDetected)
       {
           Flip();
       }
       else
       {
           movement.Set(movementSpeed * facingDirection, aliveRb.velocity.y);
           aliveRb.velocity = movement;
       }
   }

   private void ExitMovingState()
   {

   }

   //-KNOCKBACK STATE-------------------------------------------------------

   private void EnterKnockbackState()
   {
       KnockbackStartTime = Time.time;
       movement.Set(KnockbackSpeed.x * damageDirection, KnockbackSpeed.y);
       aliveRb.velocity = movement;
       aliveAnim.SetBool("Knockback", true);
   }

   private void UpdateKnockbackState()
   {
       if (Time.time >= KnockbackStartTime + KnockbackDuration)
       {
           SwitchState(State.Moving);
       }
   }

   private void ExitKnockbackState()
   {
       aliveAnim.SetBool("Knockback", false);
   }

   //-DEAD STATE-------------------------------------------------------

   private void EnterDeadState()
   {
       //Spawn chunks and blood
       aliveRb.gravityScale=0.0f;
       alive.GetComponent<CapsuleCollider2D>().enabled=false;
       movement.Set(0.0f, 0.0f);
       aliveRb.velocity = movement;
       aliveAnim.SetTrigger("IsDead");
   }

   private void UpdateDeadState()
   {

   }

   private void ExitDeadState()
   {

   }

   //--OTHERR FUNCTIONS--------------------------------------------------------------------------

   private void Damage(float[] attackDetails)
   {
       currentHealth -= attackDetails[0];

       Instantiate(hitParticle, alive.transform.position, Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f)));

       if (attackDetails[1] > alive.transform.position.x)
       {
           damageDirection = -1;
       }
       else
       {
           damageDirection = 1;
       }

       //Hit Particle

       if (currentHealth > 0.0f)
       {
           SwitchState(State.Knockback);
       }
       else if (currentHealth <= 0.0f)
       {
           SwitchState(State.Dead);
       }
   }

   private void CheckTouchDammage()
   {
       if (Time.time >= lastTouchDamageTime + touchDamageCooldown)
       {
           touchDamageBotLeft.Set(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeght / 2));
           touchDamageTopRight.Set(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeght / 2));

           Collider2D hit = Physics2D.OverlapArea(touchDamageBotLeft, touchDamageTopRight, whatisPlayer);

           if (hit != null)
           {
               lastTouchDamageTime = Time.time;
               attackDetails[0] = touchDamage;
               attackDetails[1] = alive.transform.position.x;
               hit.SendMessage("Damage", attackDetails);
           }

       }
   }

   private void Flip()
   {
       facingDirection *= -1;
       alive.transform.Rotate(0.0f, 180.0f, 0.0f);
   }


   private void SwitchState(State state)
   {
       switch(curentState)
       {
            case State.Moving:
                ExitMovingState();
                break;
            case State.Knockback:
                ExitKnockbackState();
                break;
            case State.Dead:
                ExitDeadState();
                break;
       }

       switch(state)
       {
            case State.Moving:
                EnterMovingState();
                break;
            case State.Knockback:
                EnterKnockbackState();
                break;
            case State.Dead:
                EnterDeadState();
                break;
       }

       curentState = state;
   }

   private void OnDrawGizmos() 
   {
        Gizmos.DrawLine(groundCheck.position, new Vector2(groundCheck.position.x, groundCheck.position.y - groundCheckDistance));
        Gizmos.DrawLine(WallCheck.position, new Vector2(WallCheck.position.x + wallCheckDistance, WallCheck.position.y));  
        Vector2 botLeft = new Vector2(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeght / 2));
        Vector2 botRight = new Vector2(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeght / 2));
        Vector2 topRight = new Vector2(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeght / 2));
        Vector2 topLeft = new Vector2(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeght / 2));

        Gizmos.DrawLine(botLeft, botRight);
        Gizmos.DrawLine(botRight, topRight);
        Gizmos.DrawLine(topRight, topLeft);
        Gizmos.DrawLine(topLeft, botLeft);
        
   }



}
