﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicAttackState : AttackState
{
    protected D_MagicAttackState stateData;
    protected GameObject projectile;
    protected GameObject projectile1;
    protected GameObject projectile2;
    protected MagicProjectile projectileScript;
    protected AttackDetails attackDetails;
    protected Transform[] destinationPosition;
    protected float animationDuration;

    private int gameobjectNumber;
  public MagicAttackState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Transform[] attackPositon, D_MagicAttackState stateData, Transform[] destinationPosition) : base(entity, stateMachine, animBoolName, attackPositon)
  {
      this.stateData = stateData;
      this.destinationPosition = destinationPosition;
  }

  public override void DoChecks()
    {
        base.DoChecks();
    }

   public override void Enter()
    {
        base.Enter();
        attackDetails.damageAmount = stateData.attackDamage;
        attackDetails.position = entity.aliveGO.transform.position;
        entity.SetMaterialFriction();
    }
    public override void Exit()
    {
        base.Exit();
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }
    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void TriggerAttack(int colliderNumber)
    {
        if (colliderNumber==0)
        {
            projectileScript = projectile.GetComponent<MagicProjectile>();
        }
        else if (colliderNumber==1)
        {
            projectileScript = projectile1.GetComponent<MagicProjectile>();
        }
        else 
        {
            projectileScript = projectile2.GetComponent<MagicProjectile>();
        }
        projectileScript.playAnimation();
        
        base.TriggerAttack(colliderNumber);
    }

    public override void Finishattack()
    {
        base.Finishattack();
    }

    public override void SpawnProjectile()
    {   
        base.SpawnProjectile();

            for (int i = 0; i < attackPositon.Length; i++)
            {
                if (i==0)
                {
                    projectile = GameObject.Instantiate(stateData.projectile, attackPositon[i].position, attackPositon[i].rotation);
                    destinationPosition[0]=projectile.transform.GetChild(0).transform;
                }
                else if (i==1)
                {
                    projectile1 = GameObject.Instantiate(stateData.projectile, attackPositon[i].position, attackPositon[i].rotation);
                    destinationPosition[1]=projectile1.transform.GetChild(0).transform;
                }
                else 
                {
                    projectile2 = GameObject.Instantiate(stateData.projectile, attackPositon[i].position, attackPositon[i].rotation);
                    destinationPosition[2]=projectile2.transform.GetChild(0).transform;
                }
            }
    }

    public override void MoveProjectile(int moveNumber)
    {   
        base.MoveProjectile(moveNumber);

        gameobjectNumber = moveNumber;
        if (moveNumber==0)
        {
            projectileScript = projectile.GetComponent<MagicProjectile>();
        }
        else if (moveNumber==1)
        {
            projectileScript = projectile1.GetComponent<MagicProjectile>();
        }
        else 
        {
            projectileScript = projectile2.GetComponent<MagicProjectile>();
        }
        projectileScript.StartMoving(stateData.projectileSpeed);
    }

    public override void ExplodeProjectile(int explodeNumber)
    {   
        base.ExplodeProjectile(explodeNumber);
        Collider2D[] detectedObjects = Physics2D.OverlapCircleAll(destinationPosition[explodeNumber].position,stateData.attackRadius, stateData.whatIsPlayer);
        foreach (Collider2D collider in detectedObjects)
        {
            collider.transform.SendMessage("Damage", attackDetails);
        }
    }
}
