﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttackState : AttackState
{
  protected D_MeleeAttack stateData;
  protected AttackDetails attackDetails;

  public MeleeAttackState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Transform[] attackPositon, D_MeleeAttack stateData) : base(entity, stateMachine, animBoolName, attackPositon)
  {
      this.stateData = stateData;
  }

  public override void DoChecks()
    {
        base.DoChecks();
    }

   public override void Enter()
    {
        base.Enter();
        attackDetails.damageAmount = stateData.attackDamage;
        attackDetails.position = entity.aliveGO.transform.position;
        entity.SetMaterialFriction();
    }
    public override void Exit()
    {
        base.Exit();
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }
    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
    
    public override void FinishComboAttack()
    {
        base.FinishComboAttack();
    }

    public override void TriggerAttack(int colliderNumber)
    {
        base.TriggerAttack(colliderNumber);
        Collider2D[] detectedObjects;
        try
        {
            detectedObjects = Physics2D.OverlapCircleAll(attackPositon[colliderNumber].position,stateData.attackRadius, stateData.whatIsPlayer);
        }
        catch 
        {
            detectedObjects = Physics2D.OverlapCircleAll(attackPositon[0].position,stateData.attackRadius, stateData.whatIsPlayer);
        }
        
        foreach (Collider2D collider in detectedObjects)
        {
            collider.transform.SendMessage("Damage", attackDetails);
        }
    }

    public override void Finishattack()
    {
        base.Finishattack();
    }
}
