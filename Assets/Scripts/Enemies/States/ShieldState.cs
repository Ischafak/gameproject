﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldState : State
{
    protected D_ShieldState stateData;
    protected bool performCloseRangeAction;
    protected bool isPlayerInMaxAgroRange;
    protected bool isGrounded;
    protected bool isShieldFinish;
  public ShieldState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_ShieldState stateData) : base(entity, stateMachine, animBoolName)
  {
      this.stateData = stateData;
  }

  public override void DoChecks()
    {
        base.DoChecks();
        performCloseRangeAction = entity.CheckPlayerInCloseRangeAction();
        isPlayerInMaxAgroRange = entity.CheckPlayerInMaxAgroRange();
        isGrounded = entity.CheckGround();
    }

    public override void Enter()
    {
        base.Enter();
        isShieldFinish = false;
        entity.SetMaterialFriction();
    }
    public override void Exit()
    {
        base.Exit();
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (Time.time >= StartTime + stateData.shieldTime && isGrounded)
        {
            isShieldFinish = true;
        }
    }
    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
        
    }
}
