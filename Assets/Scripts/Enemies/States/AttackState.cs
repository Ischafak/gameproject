﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : State
{
    protected Transform[] attackPositon;
    public bool isAnimationFinished;
    protected bool isPlayerInMinAgroRange;
    
  public AttackState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Transform[] attackPositon) : base(entity, stateMachine, animBoolName)
  {
    this.attackPositon = attackPositon;
  }

  public override void DoChecks()
    {
        base.DoChecks();
        isPlayerInMinAgroRange = entity.CheckPlayerInMinAgroRange();
    }

   public override void Enter()
    {
        base.Enter();
        entity.atsmm.attackState = this;
        isAnimationFinished = false;
    }
    public override void Exit()
    {
        base.Exit();
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }
    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public virtual void TriggerAttack(int colliderNumber)
    {

    }
    public virtual void FinishComboAttack()
    {
        isAnimationFinished = true;
    }
    public virtual void Finishattack()
    {
        isAnimationFinished = true;
    }

    public virtual void SpawnProjectile()
    {
        
    }

    public virtual void MoveProjectile(int moveNumber)
    {
        
    }

    public virtual void ExplodeProjectile(int moveNumber)
    {
        
    }
}
