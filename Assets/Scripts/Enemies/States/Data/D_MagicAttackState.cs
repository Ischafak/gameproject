﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newMagicAttackStateData", menuName = "Data/State Data/Magic Attack State")]
public class D_MagicAttackState : ScriptableObject
{
   public GameObject projectile;
    public float projectileDamage = 10f;
    public float projectileSpeed = 3f;
    public float attackRadius = 0.5f;
    public LayerMask whatIsPlayer;
    public float attackDamage = 10f;
}
