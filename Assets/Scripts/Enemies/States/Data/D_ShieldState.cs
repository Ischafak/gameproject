﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newShieldStateData", menuName = "Data/State Data/Shield State")]
public class D_ShieldState : ScriptableObject
{
    public float shieldCoolDown = 2f;
    public float shieldTime = 0.2f;
    public GameObject blockEffect;
}
