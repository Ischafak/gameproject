﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newFreezeStateData", menuName = "Data/State Data/Freeze State")]
public class D_FreezeState : ScriptableObject
{
    public float freezeTime = 1f;
    public float freezeBreakTime = 0.4f;
    public float freezeEndAmount = 0.005f;
    public float speed = 20f;
}
