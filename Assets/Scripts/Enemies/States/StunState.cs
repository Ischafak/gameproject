﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunState : State
{
    protected D_StunState stateData;
    protected bool isStunTimeOver;
    protected bool isGrounded;
    protected bool isMovememntStopped;
    protected bool performCloseRangeAction;
    protected bool isPlayerInMinAgroRange;
    protected GameObject stunPrefab;
  public StunState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_StunState stateData) : base(entity, stateMachine, animBoolName)
  {
      this.stateData = stateData;
  }

    public override void DoChecks()
    {
        base.DoChecks();
        isGrounded = entity.CheckGround();
        performCloseRangeAction = entity.CheckPlayerInCloseRangeAction();
        isPlayerInMinAgroRange = entity.CheckPlayerInMinAgroRange();
    }

  public override void Enter()
  {
      base.Enter();
      isStunTimeOver = false;
      entity.SetVelocity(stateData.stunKnockbackSpeed, stateData.stunKnockbackAngle, entity.lastDamageDirection);
      entity.SetMaterialFriction();
      isMovememntStopped = false;
      stunPrefab = GameObject.Instantiate(GameAssets.i.stunSprite, entity.stunPosition);
      
  }
  public override void Exit()
  {
      base.Exit();
      GameObject.Destroy(stunPrefab);
      entity.ResetStunResistance();
  }
  public override void LogicUpdate()
  {
      base.LogicUpdate();

      if (Time.time >= StartTime + stateData.stunTime)
      {
          isStunTimeOver = true;
      }
      if (isGrounded && Time.time >= StartTime + stateData.stunKnockbackTime && !isMovememntStopped)
      {
          isMovememntStopped = true;
          entity.SetVelocity(0f);
      }
  }
  public override void PhysicsUpdate()
  {
      base.PhysicsUpdate();
  }
}
