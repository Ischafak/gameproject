﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeState : State
{
    protected D_FreezeState stateData;
    protected bool isFreezeOver;
    protected float freezeEndTime;
  public FreezeState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_FreezeState stateData) : base(entity, stateMachine, animBoolName)
  {
      this.stateData = stateData;
  }

  public override void DoChecks()
    {
        base.DoChecks();
    }

  public override void Enter()
  {
      base.Enter();
      isFreezeOver = false;
      entity.SetMaterialFriction();
      entity.anim.speed = 0f;
  }
  public override void Exit()
  {
      base.Exit();
  }
  public override void LogicUpdate()
  {
    base.LogicUpdate();
    if (Time.time >= StartTime + stateData.freezeTime && !isFreezeOver)
    {
        freezeEndTime = Time.time;
        isFreezeOver = true;
    }
  }
  public override void PhysicsUpdate()
  {
      base.PhysicsUpdate();
  }
}
