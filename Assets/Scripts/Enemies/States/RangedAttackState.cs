﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAttackState : AttackState
{
    protected D_RangedAttackState stateData;
    protected GameObject projectile;
    protected Projectiles projectileScript;
  public RangedAttackState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Transform[] attackPositon, D_RangedAttackState stateData) : base(entity, stateMachine, animBoolName, attackPositon)
  {
      this.stateData = stateData;
  }

  public override void DoChecks()
    {
        base.DoChecks();
    }

   public override void Enter()
    {
        base.Enter();
        entity.SetMaterialFriction();
    }
    public override void Exit()
    {
        base.Exit();
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }
    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void TriggerAttack(int colliderNumber)
    {
        base.TriggerAttack(colliderNumber);
        projectile = GameObject.Instantiate(stateData.projectile, attackPositon[0].position, attackPositon[0].rotation);
        projectileScript = projectile.GetComponent<Projectiles>();
        projectileScript.FireProjectile(stateData.projectileSpeed, stateData.projectileTravelDistance, stateData.projectileDamage);
    }

    public override void Finishattack()
    {
        base.Finishattack();
    }
}
