﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E5_WaitSpawnForPlayerState : WaitSpawnForPlayerState
{
    private Enemy5 enemy;
    private CapsuleCollider2D bxCollider;
    private bool openChest;
  public E5_WaitSpawnForPlayerState(Entity entity, FiniteStateMachine stateMachine, string animBoolName,  Enemy5 enemy, bool openChest) : base(entity, stateMachine, animBoolName)
  {
      this.enemy = enemy;
      this.openChest = openChest;
  }

  public override void Enter()
  {
      base.Enter();
      bxCollider = entity.aliveGO.GetComponent<CapsuleCollider2D>();
   
  }
  public override void Exit()
  {
      base.Exit();
  }
  public override void LogicUpdate()
  {
      base.LogicUpdate();

      if (openChest)
      {
          entity.aliveGO.layer=9;
          stateMachine.ChangeState(enemy.moveState);
          entity.anim.SetBool("spawn2",true);
          entity.rb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
          bxCollider.enabled = true;
          bxCollider.isTrigger=false;
      }
      
  }
}
