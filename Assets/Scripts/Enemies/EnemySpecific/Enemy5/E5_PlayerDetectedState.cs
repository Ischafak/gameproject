﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E5_PlayerDetectedState : PlayerDetectedState
{
    private Enemy5 enemy;
    public float shortRangeActionTime = 0.0f;
  public E5_PlayerDetectedState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_PlayerDetected stateData, Enemy5 enemy) : base(entity, stateMachine, animBoolName, stateData)
  {
      this.enemy = enemy;
  }

  public override void Enter()
  {
      base.Enter();
   
  }
  public override void Exit()
  {
      base.Exit();
  }
  public override void LogicUpdate()
  {
      base.LogicUpdate();

      if (performCloseRangeAction && Time.time >= StartTime + shortRangeActionTime)
      {
          shortRangeActionTime = stateData.shortRangeActionTimeSet;
          stateMachine.ChangeState(enemy.chargeState);
      }

      else if (!performCloseRangeAction && performLongRangeAction)
      {
          stateMachine.ChangeState(enemy.chargeState);
      }
      else if (!isPlayerInMaxAgroRange)
      {
          stateMachine.ChangeState(enemy.lookForPLayerState);
      }
      else if (!isDetectingLedge)
      {
          entity.Flip();
          stateMachine.ChangeState(enemy.moveState);
      }
      
  }
  public override void PhysicsUpdate()
  {
      base.PhysicsUpdate();
  }
}
