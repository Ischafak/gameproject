﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy5 : Entity
{
    public E5_IdleState idleState { get; private set; }
    public E5_MoveState moveState { get; private set; }
    public E5_PlayerDetectedState playerDetectedState { get; private set;}

    public E5_ChargeState chargeState { get; private set; }

    public E5_LookForPlayerState lookForPLayerState { get; private set; }
    public E5_StunState stunState { get; private set; }
    public E5_DeadState deadState { get; private set; }
    public E5_WaitSpawnForPlayerState waitForPlayerState { get; private set; }
    public E5_FreezeState freezeState { get; private set; }

    private bool openChest = false;
   [SerializeField]
   private D_IdleState idleStateData;
   [SerializeField]
   private D_MoveState moveStateData;
   [SerializeField]
   public D_PlayerDetected playerDetectedData;

   [SerializeField]
   private D_ChargeState chargeStateData;

   [SerializeField]
   private D_LookForPlayer lookForPlayerStateData;

    [SerializeField]
   private D_StunState stunStateData;
   
   [SerializeField]
   private D_DeadState deadStateData;
   [SerializeField]
   private D_FreezeState freezeStateData;

   public override void Start() 
   {
       base.Start();
       moveState = new E5_MoveState(this, stateMachine, "move", moveStateData, this);
       idleState = new E5_IdleState(this, stateMachine, "idle", idleStateData, this);
       playerDetectedState = new E5_PlayerDetectedState(this, stateMachine, "playerDetected", playerDetectedData ,this);
       chargeState = new E5_ChargeState(this, stateMachine, "charge", chargeStateData, this);
       lookForPLayerState = new E5_LookForPlayerState(this, stateMachine, "lookForPlayer", lookForPlayerStateData, this);
       stunState = new E5_StunState(this, stateMachine, "stun", stunStateData, this);
       freezeState = new E5_FreezeState(this, stateMachine, "freeze", freezeStateData, this);
       deadState = new E5_DeadState(this, stateMachine, "dead", deadStateData, this);
       waitForPlayerState = new E5_WaitSpawnForPlayerState(this, stateMachine, "spawn" ,this, openChest);
       stateMachine.Initialize(waitForPlayerState);
   }

   public override void OnDrawGizmos()
   {
       base.OnDrawGizmos();
   }

   public override void Damage(AttackDetails attackDetails)
   {
       base.Damage(attackDetails);

       if (stateMachine.currentState == freezeState)
       {
           if (!anim.enabled)
           {
               anim.enabled = true;
           }
           stateMachine.ChangeState(lookForPLayerState);
       }
       else
       {
            if (!CheckPlayerInMaxAgroRange() && !isStunned)
            {
                 stateMachine.ChangeState(lookForPLayerState);
            }
       }
       if (isDead)
       {
           if (!anim.enabled)
           {
               SetTintColorBlack();
               anim.enabled = true;
           }
           stateMachine.ChangeState(deadState);
       }
       else if (attackDetails.freeze)
       {
           stateMachine.ChangeState(freezeState);
       }
       else if (isStunned && stateMachine.currentState != stunState)
       {
           stateMachine.ChangeState(stunState);
       }
   }
   public void openMimicChest()
   {
       openChest=true;
       waitForPlayerState = new E5_WaitSpawnForPlayerState(this, stateMachine, "spawn" ,this, openChest);
       stateMachine.ChangeState(waitForPlayerState);
   }
}
