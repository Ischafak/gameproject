﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E6_PlayerDetectedState : PlayerDetectedState
{
    private Enemy6 enemy;
  public E6_PlayerDetectedState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_PlayerDetected stateData, Enemy6 enemy) : base(entity, stateMachine, animBoolName, stateData)
  {
      this.enemy = enemy;
  }

  public override void Enter()
  {
      base.Enter();
   
  }
  public override void Exit()
  {
      base.Exit();
  }
  public override void LogicUpdate()
  {
      base.LogicUpdate();

            if (performCloseRangeAction)
            {
                if (Time.time >= enemy.shieldState.StartTime + enemy.shieldStateData.shieldCoolDown)
                {
                    stateMachine.ChangeState(enemy.shieldState);
                }
                else
                {
                    //stateMachine.ChangeState(enemy.meleeAttackState);
                }
            
            }
            else if (performLongRangeAction)
            {
                stateMachine.ChangeState(enemy.chargeState);
            }
            else if (!isPlayerInMaxAgroRange)
            {
                stateMachine.ChangeState(enemy.lookForPLayerState);
            }
  }
  public override void PhysicsUpdate()
  {
      base.PhysicsUpdate();
  }
}
