﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy6 : Entity
{
    public E6_IdleState idleState { get; private set; }
    public E6_MoveState moveState { get; private set; }
    public E6_PlayerDetectedState playerDetectedState { get; private set;}

    public E6_ChargeState chargeState { get; private set; }

    public E6_LookForPlayerState lookForPLayerState { get; private set; }
    public E6_MeleeAttackState meleeAttackState { get; private set; }
    public E6_StunState stunState { get; private set; }
    public E6_DeadState deadState { get; private set; }
    public E6_ShieldState shieldState { get; private set; }
    public E6_FreezeState freezeState { get; private set; }

   [SerializeField]
   private D_IdleState idleStateData;
   [SerializeField]
   private D_MoveState moveStateData;
   [SerializeField]
   public D_PlayerDetected playerDetectedData;

   [SerializeField]
   private D_ChargeState chargeStateData;

   [SerializeField]
   private D_LookForPlayer lookForPlayerStateData;

   [SerializeField]
   private D_MeleeAttack meleeAttackStateData;

   [SerializeField]
   private Transform[] meleeAttackPosition;
   [SerializeField]
   private D_StunState stunStateData;
   [SerializeField]
   private D_DeadState deadStateData;
   [SerializeField]
   private D_FreezeState freezeStateData;
   [SerializeField]
   public D_ShieldState shieldStateData;
   [HideInInspector]
   public bool shieldSituation = false;
   [SerializeField]
   private Transform shieldPos;
   private PlayerControllerX playerController;
   private PlayerAttack playerAttack;
   public float flipWaitDuration;
   private float flipwaitStart;
   private float lastDamagePosition;
   private float damageMoveValue = 2f;
   [HideInInspector]
    public State stateBeforeFreeze;

   public override void Start() 
   {
       base.Start();
       playerController = GameObject.Find("Player").GetComponent<PlayerControllerX>();
       playerAttack = GameObject.Find("Player").GetComponent<PlayerAttack>();
       moveState = new E6_MoveState(this, stateMachine, "move", moveStateData, this);
       idleState = new E6_IdleState(this, stateMachine, "idle", idleStateData, this);
       playerDetectedState = new E6_PlayerDetectedState(this, stateMachine, "playerDetected", playerDetectedData ,this);
       chargeState = new E6_ChargeState(this, stateMachine, "charge", chargeStateData, this);
       lookForPLayerState = new E6_LookForPlayerState(this, stateMachine, "lookForPlayer", lookForPlayerStateData, this);
       meleeAttackState = new E6_MeleeAttackState(this, stateMachine, "meleeAttack", meleeAttackPosition, meleeAttackStateData, this);
       stunState = new E6_StunState(this, stateMachine, "stun", stunStateData, this);
       freezeState = new E6_FreezeState(this, stateMachine, "freeze", freezeStateData, this);
       deadState = new E6_DeadState(this, stateMachine, "dead", deadStateData, this);
       shieldState = new E6_ShieldState(this, stateMachine, "shield", shieldStateData, this);
       stateMachine.Initialize(moveState);
   }

   public override void OnDrawGizmos()
   {
       base.OnDrawGizmos();
       for (int i = 0; i < meleeAttackPosition.Length; i++)
       {
           Gizmos.DrawWireSphere(meleeAttackPosition[i].position, meleeAttackStateData.attackRadius);
       }
       
   }

   public override void Damage(AttackDetails attackDetails)
   {
       if (stateMachine.currentState == freezeState)
       {
           if (anim.speed==0f)
           {
               anim.speed=1f;
           }
           stateMachine.ChangeState(lookForPLayerState);
       }
       else
       {
            if (stateMachine.currentState != shieldState && !CheckPlayerInMaxAgroRange() && !isStunned)
            {
                 stateMachine.ChangeState(lookForPLayerState);
            }
       }

       if (!shieldSituation)
       {
            base.Damage(attackDetails);
            if (isDead)
            {
                if (anim.speed==0f)
                {
                    SetTintColorBlack();
                    anim.speed=1f;
                }
                stateMachine.ChangeState(deadState);
            }
            else if (attackDetails.freeze)
            {
                stateBeforeFreeze = stateMachine.currentState;
                stateMachine.ChangeState(freezeState);
            }
            else if (isStunned && stateMachine.currentState != stunState)
            {
                stateMachine.ChangeState(stunState);
            }
       }
       else
       {
           if (playerController.GetFacingDirection() == facingDirection)
           {
                base.Damage(attackDetails);
                if (isDead)
                {
                    stateMachine.ChangeState(deadState);
                }
                else if (attackDetails.freeze)
                {
                    stateMachine.ChangeState(freezeState);
                }
                else if (isStunned && stateMachine.currentState != stunState)
                {
                    stateMachine.ChangeState(stunState);
                }
                if (Time.time>=flipwaitStart+flipWaitDuration && !isStunned)
                {
                    Flip();
                    flipwaitStart = Time.time;
                } 
           }
           else
           {
               playerAttack.transform.SendMessage("ShieldBlock");
               Instantiate(shieldStateData.blockEffect,shieldPos.transform.position, Quaternion.Euler(0.0f,0.0f, Random.Range(0.0f, 360.0f)));
               if (attackDetails.crit)
               {
                    SetMaterialNormal();
                    if (attackDetails.position.x > aliveGO.transform.position.x)
                    {
                        lastDamagePosition = -1;
                    }
                    else
                    {
                        lastDamagePosition = 1;
                    }
                    Vector2 forceToAdd = new Vector2(damageMoveValue * lastDamagePosition, 1f);
                    rb.AddForce(forceToAdd, ForceMode2D.Impulse);
                    SetMaterialFriction();
               }
               
           }
       }
   }
}
