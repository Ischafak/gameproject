﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E6_FreezeState : FreezeState
{
    private Enemy6 enemy;
    Vector2 startingPos;
  public E6_FreezeState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_FreezeState stateData, Enemy6 enemy) : base(entity, stateMachine, animBoolName, stateData)
  {
      this.enemy = enemy;
  }

   public override void Enter()
    {
        base.Enter();
        
    }
    public override void Exit()
    {
        base.Exit();
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (entity.CheckGround())
        {
            startingPos.x = entity.aliveGO.transform.position.x;
        }
        if (isFreezeOver)
        {
            if (Time.time <= freezeEndTime + stateData.freezeBreakTime)
            {
                entity.aliveGO.transform.position = new Vector3(startingPos.x + Mathf.Sin(Time.time * stateData.speed) * stateData.freezeEndAmount ,entity.aliveGO.transform.position.y ,entity.aliveGO.transform.position.z);
            }
            else
            {
                entity.SetTintColorBlack();
                entity.anim.enabled = true;
                stateMachine.ChangeState(enemy.lookForPLayerState);
            }
        }
    }
    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
