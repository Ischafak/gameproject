﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E6_ShieldState : ShieldState
{
    private Enemy6 enemy;
  public E6_ShieldState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_ShieldState stateData, Enemy6 enemy) : base(entity, stateMachine, animBoolName, stateData)
  {
      this.enemy = enemy;
  }

  public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
        enemy.shieldSituation = true;
    }
    public override void Exit()
    {
        base.Exit();
        enemy.shieldSituation = false;
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isShieldFinish)
        {
            entity.anim.SetBool("shield", false);
            if (isPlayerInMaxAgroRange && performCloseRangeAction)
            {
                stateMachine.ChangeState(enemy.meleeAttackState);
            }
            else if (isPlayerInMaxAgroRange && !performCloseRangeAction)
            {
                stateMachine.ChangeState(enemy.chargeState);
            }
            else if (!isPlayerInMaxAgroRange)
            {
                stateMachine.ChangeState(enemy.lookForPLayerState);
            }
        }
    }
    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
