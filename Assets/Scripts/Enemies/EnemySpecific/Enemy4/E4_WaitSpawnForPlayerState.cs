﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E4_WaitSpawnForPlayerState : WaitSpawnForPlayerState
{
    private Enemy4 enemy;
    private CapsuleCollider2D bxCollider;
  public E4_WaitSpawnForPlayerState(Entity entity, FiniteStateMachine stateMachine, string animBoolName,  Enemy4 enemy) : base(entity, stateMachine, animBoolName)
  {
      this.enemy = enemy;
  }

  public override void Enter()
  {
      base.Enter();
      bxCollider = entity.aliveGO.GetComponent<CapsuleCollider2D>();
      bxCollider.enabled = false;
   
  }
  public override void Exit()
  {
      base.Exit();
  }
  public override void LogicUpdate()
  {
      base.LogicUpdate();

      if (performCloseRangeAction)
      {
          entity.aliveGO.layer=9;
          stateMachine.ChangeState(enemy.moveState);
          entity.anim.SetBool("spawn2",true);
          entity.rb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
          bxCollider.enabled = true;
      }
      
  }
  public override void PhysicsUpdate()
  {
      base.PhysicsUpdate();
  }
}
