﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E4_MeleeAttackState : MeleeAttackState
{
    private Enemy4 enemy;
    protected bool performCloseRangeAction;
  public E4_MeleeAttackState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Transform[] attackPositon, D_MeleeAttack stateData, Enemy4 enemy) : base(entity, stateMachine, animBoolName, attackPositon, stateData)
  {
      this.enemy = enemy;
  }

  public override void DoChecks()
    {
        base.DoChecks();
        performCloseRangeAction = entity.CheckPlayerInCloseRangeAction();
    }

   public override void Enter()
    {
        base.Enter();
    }
    public override void Exit()
    {
        base.Exit();
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (isAnimationFinished)
        {
            if (isPlayerInMinAgroRange)
            {
                stateMachine.ChangeState(enemy.playerDetectedState);
            }
            else
            {
                stateMachine.ChangeState(enemy.lookForPLayerState);
            }
        }
    }
    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void TriggerAttack(int colliderNumber)
    {
        base.TriggerAttack(colliderNumber);
    }
    public override void FinishComboAttack()
    {
        base.FinishComboAttack();
        entity.anim.SetBool("meleeAttack2",false);
    }
    public override void Finishattack()
    {
        base.Finishattack();
        if (performCloseRangeAction)
        {
            entity.anim.SetBool("meleeAttack2",true);
        }
    }
}
