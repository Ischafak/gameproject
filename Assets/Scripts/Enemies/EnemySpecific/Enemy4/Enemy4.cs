﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy4 : Entity
{
    public E4_IdleState idleState { get; private set; }
    public E4_MoveState moveState { get; private set; }
    public E4_PlayerDetectedState playerDetectedState { get; private set;}

    public E4_ChargeState chargeState { get; private set; }

    public E4_LookForPlayerState lookForPLayerState { get; private set; }
    public E4_MeleeAttackState meleeAttackState { get; private set; }
    public E4_StunState stunState { get; private set; }
    public E4_DeadState deadState { get; private set; }
    public E4_WaitSpawnForPlayerState waitForPlayerState { get; private set; }
    public E4_FreezeState freezeState { get; private set; }

   [SerializeField]
   private D_IdleState idleStateData;
   [SerializeField]
   private D_MoveState moveStateData;
   [SerializeField]
   public D_PlayerDetected playerDetectedData;

   [SerializeField]
   private D_ChargeState chargeStateData;

   [SerializeField]
   private D_LookForPlayer lookForPlayerStateData;

   [SerializeField]
   private D_MeleeAttack meleeAttackStateData;

   [SerializeField]
   private Transform[] meleeAttackPosition;
    [SerializeField]
   private D_StunState stunStateData;
   [SerializeField]
   private D_DeadState deadStateData;
   [SerializeField]
   private D_FreezeState freezeStateData;
   [HideInInspector]
    public State stateBeforeFreeze;

   public override void Start() 
   {
       base.Start();
       moveState = new E4_MoveState(this, stateMachine, "move", moveStateData, this);
       idleState = new E4_IdleState(this, stateMachine, "idle", idleStateData, this);
       playerDetectedState = new E4_PlayerDetectedState(this, stateMachine, "playerDetected", playerDetectedData ,this);
       chargeState = new E4_ChargeState(this, stateMachine, "charge", chargeStateData, this);
       lookForPLayerState = new E4_LookForPlayerState(this, stateMachine, "lookForPlayer", lookForPlayerStateData, this);
       meleeAttackState = new E4_MeleeAttackState(this, stateMachine, "meleeAttack", meleeAttackPosition, meleeAttackStateData, this);
       stunState = new E4_StunState(this, stateMachine, "stun", stunStateData, this);
       freezeState = new E4_FreezeState(this, stateMachine, "freeze", freezeStateData, this);
       deadState = new E4_DeadState(this, stateMachine, "dead", deadStateData, this);
       waitForPlayerState = new E4_WaitSpawnForPlayerState(this, stateMachine, "spawn" ,this);
       stateMachine.Initialize(waitForPlayerState);
   }

   public override void OnDrawGizmos()
   {
       base.OnDrawGizmos();
       Gizmos.DrawWireSphere(meleeAttackPosition[0].position, meleeAttackStateData.attackRadius);
   }

   public override void Damage(AttackDetails attackDetails)
   {
       base.Damage(attackDetails);
       
       if (stateMachine.currentState == freezeState)
       {
           if (anim.speed==0f)
           {
               anim.speed=1f;
           }
           stateMachine.ChangeState(lookForPLayerState);
       }
       else
       {
            if (!CheckPlayerInMaxAgroRange() && !isStunned)
            {
                 stateMachine.ChangeState(lookForPLayerState);
            }
       }
       if (isDead)
       {
           if (anim.speed==0f)
           {
               SetTintColorBlack();
               anim.speed=1f;
           }
           stateMachine.ChangeState(deadState);
       }
       else if (attackDetails.freeze)
       {
           stateBeforeFreeze = stateMachine.currentState;
           stateMachine.ChangeState(freezeState);
       }
       else if (isStunned && stateMachine.currentState != stunState)
       {
           stateMachine.ChangeState(stunState);
       }
   }
}
