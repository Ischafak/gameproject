﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : Entity
{
  public E2_IdleState idleState { get; private set; }
  public E2_MoveState moveState { get; private set; }

  public E2_PlayerDetectedState playerDetectedState { get; private set; }

  public E2_MeleeAttackState meleeAttackState { get; private set; }
  public E2_LookForPLayerState lookForPLayerState { get; private set; }

  public E2_StunState stunState { get; private set; }
  public E2_DeadState deadState { get; private set; }
  public E2_DodgeState dodgeState { get; private set; }
  public E2_RangedAttackState rangedAttackState { get; private set; }
  public E2_FreezeState freezeState { get; private set; }

  [SerializeField]
   private D_IdleState idleStateData;
   [SerializeField]
   private D_MoveState moveStateData;
   [SerializeField]
   private D_PlayerDetected playerDetectedStateData;
   [SerializeField]
   private D_MeleeAttack meleeAttackStateData;
   [SerializeField]
   private Transform[] meleeAttackPosition;
   [SerializeField]
   private D_LookForPlayer lookForPLayerStateData;
   [SerializeField]
   private D_StunState stunStateData;
    [SerializeField]
   private D_DeadState deadStateData;
   [SerializeField]
   private D_FreezeState freezeStateData;
   [SerializeField]
   public D_DodgeState dodgeStateData;
   [SerializeField]
   public D_RangedAttackState rangedAttackStateData;
   [SerializeField]
   public Transform[] rangedAttackPosition;
   [HideInInspector]
    public State stateBeforeFreeze;

   public override void Start() 
   {
       base.Start();
       moveState = new E2_MoveState(this, stateMachine, "move", moveStateData, this);
       idleState = new E2_IdleState(this, stateMachine, "idle", idleStateData, this);
       playerDetectedState = new E2_PlayerDetectedState(this, stateMachine, "playerDetected", playerDetectedStateData, this);
       meleeAttackState = new E2_MeleeAttackState(this, stateMachine, "meleeAttack", meleeAttackPosition, meleeAttackStateData, this);
       lookForPLayerState = new E2_LookForPLayerState(this, stateMachine, "lookForPlayer", lookForPLayerStateData, this);
       stunState = new E2_StunState(this, stateMachine, "stun", stunStateData, this);
       freezeState = new E2_FreezeState(this, stateMachine, "freeze", freezeStateData, this);
       deadState = new E2_DeadState(this, stateMachine, "dead", deadStateData, this);
       dodgeState = new E2_DodgeState(this, stateMachine, "dodge", dodgeStateData, this);
       rangedAttackState = new E2_RangedAttackState(this, stateMachine, "rangedAttack", rangedAttackPosition, rangedAttackStateData, this);
       stateMachine.Initialize(moveState);
   }

   public override void OnDrawGizmos()
   {
       base.OnDrawGizmos();
       Gizmos.DrawWireSphere(meleeAttackPosition[0].position, meleeAttackStateData.attackRadius);
   }

   public override void Damage(AttackDetails attackDetails)
   {
       base.Damage(attackDetails);  

       if (stateMachine.currentState == freezeState)
       {
           if (anim.speed==0f)
           {
               anim.speed=1f;
           }
           stateMachine.ChangeState(lookForPLayerState);
       }
       else
       {
            if (!CheckPlayerInMaxAgroRange())
            {
                stateMachine.ChangeState(lookForPLayerState);  
            }
       }
       if (isDead)
       {
           if (anim.speed==0f)
           {
               SetTintColorBlack();
               anim.speed=1f;
           }
           stateMachine.ChangeState(deadState);
       }  
       else if (attackDetails.freeze)
       {
           stateBeforeFreeze = stateMachine.currentState;
           stateMachine.ChangeState(freezeState);
       }
       else if (isStunned && stateMachine.currentState != stunState)
       {
           stateMachine.ChangeState(stunState);
       }
       else if (CheckPlayerInMinAgroRange() && !isStunned)
       {
           stateMachine.ChangeState(rangedAttackState);
       }
   }
}
