﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E3_LookForPLayerState : LookForPlayerState
{
    private Enemy3 enemy;
  public E3_LookForPLayerState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_LookForPlayer stateData, Enemy3 enemy) : base(entity, stateMachine, animBoolName, stateData)
  {
      this.enemy = enemy;
  }
  public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (isPLayerInMinAgroRange)
        {
            stateMachine.ChangeState(enemy.playerDetectedState);
        }
        else if (isAllTurnsTimeDone)
        {
            stateMachine.ChangeState(enemy.moveState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
