﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3 : Entity
{
  public E3_IdleState idleState { get; private set; }
  public E3_MoveState moveState { get; private set; }

  public E3_PlayerDetectedState playerDetectedState { get; private set; }

  public E3_MeleeAttackState meleeAttackState { get; private set; }
  public E3_LookForPLayerState lookForPLayerState { get; private set; }

  public E3_StunState stunState { get; private set; }
  public E3_DeadState deadState { get; private set; }
  public E3_FreezeState freezeState { get; private set; }
  public E3_MagicAttackState magicAttackState { get; private set; }

  [SerializeField]
   private D_IdleState idleStateData;
   [SerializeField]
   private D_MoveState moveStateData;
   [SerializeField]
   private D_PlayerDetected playerDetectedStateData;
   [SerializeField]
   private D_MeleeAttack meleeAttackStateData;
   [SerializeField]
   private Transform[] meleeAttackPosition;
   [SerializeField]
   private D_LookForPlayer lookForPLayerStateData;
   [SerializeField]
   private D_StunState stunStateData;
    [SerializeField]
   private D_DeadState deadStateData;
   [SerializeField]
   private D_FreezeState freezeStateData;

   [SerializeField]
   public D_MagicAttackState magicAttackStateData;
   [SerializeField]
   public Transform[] rangedAttackPosition;
   public Transform[] destinationPosition;
   [HideInInspector]
    public State stateBeforeFreeze;


   public override void Start() 
   {
       base.Start();
       moveState = new E3_MoveState(this, stateMachine, "move", moveStateData, this);
       idleState = new E3_IdleState(this, stateMachine, "idle", idleStateData, this);
       playerDetectedState = new E3_PlayerDetectedState(this, stateMachine, "playerDetected", playerDetectedStateData, this);
       meleeAttackState = new E3_MeleeAttackState(this, stateMachine, "meleeAttack", meleeAttackPosition, meleeAttackStateData, this);
       lookForPLayerState = new E3_LookForPLayerState(this, stateMachine, "lookForPlayer", lookForPLayerStateData, this);
       stunState = new E3_StunState(this, stateMachine, "stun", stunStateData, this);
       freezeState = new E3_FreezeState(this, stateMachine, "freeze", freezeStateData, this);
       deadState = new E3_DeadState(this, stateMachine, "dead", deadStateData, this);
       magicAttackState = new E3_MagicAttackState(this, stateMachine, "rangedAttack", rangedAttackPosition, magicAttackStateData, this, destinationPosition);
       stateMachine.Initialize(moveState);
   }

   public override void OnDrawGizmos()
   {
       base.OnDrawGizmos();
       for (int i = 0; i < meleeAttackPosition.Length; i++)
       {
           Gizmos.DrawWireSphere(meleeAttackPosition[i].position, meleeAttackStateData.attackRadius);
       }
       
   }

   public override void Damage(AttackDetails attackDetails)
   {
       base.Damage(attackDetails); 
       if (stateMachine.currentState == freezeState)
       {
            if (anim.speed==0f)
            {
               anim.speed=1f;
            }
            stateMachine.ChangeState(lookForPLayerState);
       }
       else
       {
            if (!CheckPlayerInMaxAgroRange())
            {
                stateMachine.ChangeState(lookForPLayerState);
            }
       }
       if (isDead)
       {
           if (anim.speed==0f)
           {
               SetTintColorBlack();
               anim.speed=1f;
           }
           stateMachine.ChangeState(deadState);
       }  
       else if (attackDetails.freeze)
       {
           stateBeforeFreeze = stateMachine.currentState;
           stateMachine.ChangeState(freezeState);
       }
       else if (isStunned && stateMachine.currentState != stunState)
       {
           stateMachine.ChangeState(stunState);
       }
       else if (CheckPlayerInMinAgroRange() && !isStunned)
       {
           stateMachine.ChangeState(magicAttackState);
       }
       /*
       else if (!CheckPlayerInMinAgroRange())
       {
           lookForPLayerState.SetTurnImmmmediately(true);
           stateMachine.ChangeState(lookForPLayerState);
       }
       */
   }
}
