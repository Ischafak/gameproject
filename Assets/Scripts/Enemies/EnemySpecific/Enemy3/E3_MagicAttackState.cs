﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E3_MagicAttackState : MagicAttackState
{
    private Enemy3 enemy;
  public E3_MagicAttackState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, Transform[] attackPositon, D_MagicAttackState stateData, Enemy3 enemy,Transform[] destinationPosition ) : base(entity, stateMachine, animBoolName, attackPositon, stateData, destinationPosition)
  {
      this.enemy = enemy;
  }

  public override void DoChecks()
    {
        base.DoChecks();
    }

   public override void Enter()
    {
        base.Enter();
    }
    public override void Exit()
    {
        base.Exit();
    }
    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (isAnimationFinished)
        {
            if (isPlayerInMinAgroRange)
            {
                stateMachine.ChangeState(enemy.playerDetectedState);
            }
            else
            {
                stateMachine.ChangeState(enemy.lookForPLayerState);
            }
        }
    }
    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void TriggerAttack(int colliderNumber)
    {
        base.TriggerAttack(colliderNumber);
    }

    public override void Finishattack()
    {
        base.Finishattack();
    }

    public override void SpawnProjectile()
    {
        base.SpawnProjectile();
    }
}
