﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1 : Entity
{
   public E1_IdleState idleState { get; private set; }
   public E1_MoveState moveState { get; private set; }
   public E1_PlayerDetectedState playerDetectedState { get; private set;}

    public E1_ChargeState chargeState { get; private set; }

    public E1_LookForPlayerState lookForPLayerState { get; private set; }
    public E1_MeleeAttackState meleeAttackState { get; private set; }
    public E1_StunState stunState { get; private set; }
    public E1_DeadState deadState { get; private set; }
    public E1_FreezeState freezeState { get; private set; }

   [SerializeField]
   private D_IdleState idleStateData;
   [SerializeField]
   private D_MoveState moveStateData;
   [SerializeField]
   public D_PlayerDetected playerDetectedData;

   [SerializeField]
   private D_ChargeState chargeStateData;

   [SerializeField]
   private D_LookForPlayer lookForPlayerStateData;

   [SerializeField]
   private D_MeleeAttack meleeAttackStateData;

   [SerializeField]
   private Transform[] meleeAttackPosition;
    [SerializeField]
   private D_StunState stunStateData;
   [SerializeField]
   private D_DeadState deadStateData;
   [SerializeField]
   private D_FreezeState freezeStateData;
   
   [HideInInspector]
    public State stateBeforeFreeze;
   public override void Start() 
   {
       base.Start();
       moveState = new E1_MoveState(this, stateMachine, "move", moveStateData, this);
       idleState = new E1_IdleState(this, stateMachine, "idle", idleStateData, this);
       playerDetectedState = new E1_PlayerDetectedState(this, stateMachine, "playerDetected", playerDetectedData ,this);
       chargeState = new E1_ChargeState(this, stateMachine, "charge", chargeStateData, this);
       lookForPLayerState = new E1_LookForPlayerState(this, stateMachine, "lookForPlayer", lookForPlayerStateData, this);
       meleeAttackState = new E1_MeleeAttackState(this, stateMachine, "meleeAttack", meleeAttackPosition, meleeAttackStateData, this);
       stunState = new E1_StunState(this, stateMachine, "stun", stunStateData, this);
       freezeState = new E1_FreezeState(this, stateMachine, "freeze", freezeStateData, this);
       deadState = new E1_DeadState(this, stateMachine, "dead", deadStateData, this);
       stateMachine.Initialize(moveState);
   }

   public override void OnDrawGizmos()
   {
       base.OnDrawGizmos();
       Gizmos.DrawWireSphere(meleeAttackPosition[0].position, meleeAttackStateData.attackRadius);
   }

   public override void Damage(AttackDetails attackDetails)
   {
       base.Damage(attackDetails);

       if (stateMachine.currentState == freezeState)
       {
           if (anim.speed==0f)
           {
               anim.speed=1f;
           }
           stateMachine.ChangeState(lookForPLayerState);
       }
       else
       {
            if (!CheckPlayerInMaxAgroRange() && !isStunned)
            {
                stateMachine.ChangeState(lookForPLayerState);   
            }
       }
       if (isDead)
       {
           if (anim.speed==0f)
           {
               SetTintColorBlack();
               anim.speed=1f;
           }
           stateMachine.ChangeState(deadState);
       }
       else if (attackDetails.freeze)
       {
           stateBeforeFreeze = stateMachine.currentState;
           stateMachine.ChangeState(freezeState);
       }
       else if (isStunned && stateMachine.currentState != stunState)
       {
           
           stateMachine.ChangeState(stunState);
       }
       
   }
}
