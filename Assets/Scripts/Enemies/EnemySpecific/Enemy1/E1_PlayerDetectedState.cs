﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E1_PlayerDetectedState : PlayerDetectedState
{
    private Enemy1 enemy;
    public float shortRangeActionTime = 0.0f;
  public E1_PlayerDetectedState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_PlayerDetected stateData, Enemy1 enemy) : base(entity, stateMachine, animBoolName, stateData)
  {
      this.enemy = enemy;
  }

  public override void Enter()
  {
      base.Enter();
   
  }
  public override void Exit()
  {
      base.Exit();
  }
  public override void LogicUpdate()
  {
      base.LogicUpdate();

      if (performCloseRangeAction && Time.time >= StartTime + shortRangeActionTime)
      {
          shortRangeActionTime = stateData.shortRangeActionTimeSet;
          stateMachine.ChangeState(enemy.meleeAttackState);
      }

      else if (!performCloseRangeAction && performLongRangeAction)
      {
          stateMachine.ChangeState(enemy.chargeState);
      }
      else if (!isPlayerInMaxAgroRange)
      {
          stateMachine.ChangeState(enemy.lookForPLayerState);
      }
      else if (!isDetectingLedge)
      {
          entity.Flip();
          stateMachine.ChangeState(enemy.moveState);
      }
      
  }
  public override void PhysicsUpdate()
  {
      base.PhysicsUpdate();
  }
}
