﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{   

 public float spriteBlinkingTimer = 0.0f;
 public float spriteBlinkingMiniDuration = 0.1f;
 public float spriteBlinkingTotalTimer = 0.0f;
 public float spriteBlinkingTotalDuration = 1.0f;
 public bool startBlinking = false;


    private SpriteRenderer m_SpriteRenderer;
    private PlayerAttack playerAttack;
    public ParticleSystem trailEffect;
    public float movementSpeed = 6.0f;
    public float jumpForce = 16.0f;
    public float groundCheckRadius;
    public Transform groundCheck;
    public Transform wallCheck;
    public LayerMask whatIsGround;
    public LayerMask whatIsWall;
    
    public LayerMask whatIsLedge;
    public int amountOfJumps = 1;
    public float wallCheckDistance;
    public float wallSlideSpeed;
    public float movementForceInAir;
    public float airDragMultiplier = 0.95f;
    public float variableJumpHeightMultiplier = 0.5f;
    public Vector2 wallHopDirection;
    public Vector2 wallJumpDirection;  
    public float wallJumpForce; 
    public float jumpTimerSet = 0.15f;  
    public float turnTimerSet = 0.1f; 
    public float wallJumpTimerSet = 0.5f;
    public Transform ledgeCheck;
    public Transform WallParticleSpawnPoint;
    public float ledgeClimbXOffset1 = 0f;
    public float ledgeClimbYOffset1 = 0f;
    public float ledgeClimbXOffset2 = 0f;
    public float ledgeClimbYOffset2 = 0f;
    public float maxSlideTime;
    public float slideSpeed;
    public float slideCoolDown;
    Vector2 m_NextMovement;
    public bool isGrounded;


    
    private float slideTime;
    private bool isSliding;
    private bool knockback;
    private bool grabTime = false;
    private bool canClimbLedge = false;
    private bool canGrabLedge = false;
    private bool ledgeDetected;
    private bool hasWallJumped;
    private bool isTouchingLedge;
    private float ledgeClimbYOffset1G;
    
    private Vector2 ledgePosBot;
    private Vector2 ledgePos1;
    private Vector2 ledgePos2;
    
    
    
    private int lastWallJumpDirection;
    
    private float wallJumpTimer;
    private float turnTimer;
    public bool canMove;
    public bool canFlip;
    private bool checkJumpMultiplier;
    private float jumpTimer;
    private float timeBtwTrail;
    private float knockbackStartTime;
    [SerializeField]
    private float knockbackDuration;
    private float startTimeBtwTrail=0.5f;
    private bool isAttemptingToJump;
    private int facingDirection = 1;
    private bool isTouchingWall;
    private int amountOfJumpsLeft;
    [SerializeField]
    private Vector2 knockbackSpeed;
    [SerializeField]
    private GameObject wallSlideEffect;
    
    private Rigidbody2D rb;
    private Animator anim;
    private bool isWallSliding;
    private bool isFacingRight=true;
    private bool canNormalJump;
    private bool canWallJump;
    private float movementInputDirection;
    private bool startSpawnWallEffect = false;
    void Start()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        playerAttack = GetComponent<PlayerAttack>();
        trailEffect.Stop();
        ledgeClimbYOffset1G = ledgeClimbYOffset1;
        rb=GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        amountOfJumpsLeft=amountOfJumps;
        wallHopDirection.Normalize();
        wallJumpDirection.Normalize();
    }

    // Update is called once per frame
    void Update()
    {
        if(startBlinking == true)
         { 
             SpriteBlinkingEffect();
         }
        CheckInput();
        CheckMovementDirection();
        UpdateAnimations();
        CheckIfCanJump();
        CheckIfWallSliding();
        CheckJump();
        CheckLedgeClimb();
        CheckGrabToClimb();
        CheckSlide();
        CheckKnockback();
    }

    private void FixedUpdate() 
    {
        m_NextMovement = Vector2.zero;
        TrailEffectGenerator();
        ApplyMovement();
        CheckSurroundings();
    }

    public int GetFacingDirection()
    {
        return facingDirection;
    }
    private void TrailEffectGenerator()
    {
        float hor = Input.GetAxis ("Horizontal");
        if (Mathf.Abs(hor)!=0)
		{
			if (!trailEffect.isPlaying && isGrounded)
			{
				trailEffect.Play();
			}
			if (!isGrounded)
			{
				trailEffect.Stop();
			}
			
		}
		else
		{
			trailEffect.Stop();
		}
    }
    private void CheckIfWallSliding()
    {
        if (isTouchingWall && movementInputDirection == facingDirection && rb.velocity.y < 0)
        {
            isWallSliding =true;
        }
        else
        {
            if (startSpawnWallEffect)
            {
                CancelInvoke("spawnWallSlideEffect");
                startSpawnWallEffect = false;
            }
            
            isWallSliding=false;
        }
    }

    private void CheckGrabToClimb()
    {
        if (canGrabLedge == true)
        {
            rb.gravityScale=0f;
            rb.velocity = new Vector2(0.0f, 0.0f);
            canMove=false;
            canFlip=false;
            if (Input.GetButtonDown("Jump"))
            {    
                canClimbLedge=true;
                canGrabLedge=false;
                grabTime=true;
                anim.SetBool("canClimbLedge",canClimbLedge);
                anim.SetBool("canGrabLedge",canGrabLedge);
            }
            if (movementInputDirection!=0 && facingDirection==-movementInputDirection)
            {
                rb.gravityScale=1f;
                canMove=true;
                canFlip=true;
                Vector2 forceToAdd = new Vector2(wallJumpForce * wallJumpDirection.x * movementInputDirection, wallJumpForce * wallJumpDirection.y);
                rb.AddForce(forceToAdd, ForceMode2D.Force);
                canGrabLedge=false;
                anim.SetBool("canGrabLedge",canGrabLedge);
                grabTime=true;
                ledgeDetected=false;
            }
        }
    }
    

    private void CheckLedgeClimb()
    {
        if (ledgeDetected && !canGrabLedge && grabTime==false && !isWallSliding)
        {
            
            canGrabLedge=true;

            if (isFacingRight)
            {
                ledgePos1 = new Vector2(ledgePosBot.x + wallCheckDistance - ledgeClimbXOffset1 ,ledgePosBot.y + ledgeClimbYOffset1);
                ledgePos2 = new Vector2(ledgePosBot.x + wallCheckDistance + ledgeClimbXOffset2 ,ledgePosBot.y + ledgeClimbYOffset2);
                
            }
            else
            {
                ledgePos1 = new Vector2(ledgePosBot.x - -wallCheckDistance + ledgeClimbXOffset1 ,ledgePosBot.y + ledgeClimbYOffset1);
                ledgePos2 = new Vector2(ledgePosBot.x - -wallCheckDistance - ledgeClimbXOffset2 ,ledgePosBot.y + ledgeClimbYOffset2);
            }


            canMove=false;
            canFlip=false;
            
            anim.SetBool("canGrabLedge",canGrabLedge);
               
        }
        
        if (canGrabLedge && grabTime==false)
        {
            transform.position = ledgePos1;
        }
    }

    private void CheckIfCanJump()
    {
        if (isGrounded && rb.velocity.y <=0.01f)
        {
            if (ledgeDetected)
            {
                ledgeDetected = false;
            }
            
            amountOfJumpsLeft = amountOfJumps; 
        }
        if (isTouchingWall)
        {
            canWallJump=true;
        }
        if (amountOfJumpsLeft <= 0 || canGrabLedge)
        {
            canNormalJump=false;
        }
        else
        {
            if (!isTouchingWall)
            {
                canNormalJump=true;
            }
            
        }
      
    }
 
    public void FinishLedgeClimb()
    {
        canClimbLedge=false;
        transform.position = ledgePos2;
        canMove = true;
        canFlip = true;
        ledgeDetected = false;
        anim.SetBool("canClimbLedge",canClimbLedge);
        rb.gravityScale = 1f;
    }
    private void CheckSurroundings()
    {   
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
        isTouchingWall = Physics2D.Raycast(wallCheck.position, transform.right, wallCheckDistance, whatIsWall);
        isTouchingLedge = Physics2D.Raycast(ledgeCheck.position, transform.right, wallCheckDistance, whatIsLedge);

        if (isTouchingWall && !isTouchingLedge && !ledgeDetected)
        {
            //if && (facingDirection == movementInputDirection)
            ledgeDetected=true;
            ledgePosBot = wallCheck.position;
        }
        
    }

    private void CheckMovementDirection()
    {
        if (isFacingRight && movementInputDirection < 0 )
        {
            Flip();
        }
        else if(!isFacingRight && movementInputDirection > 0)
        {
            Flip();
        }
    }

    private void UpdateAnimations()
    {
        if (!playerAttack.isAttacking)
        {
            anim.SetFloat("Speed",Mathf.Abs(movementInputDirection));
        }
        else
        {
            anim.SetFloat("Speed",0f);
        }
        
        anim.SetBool("IsGrounded",isGrounded);
        anim.SetFloat("yVelocity",rb.velocity.y);
        anim.SetBool("IsWallSliding",isWallSliding);
    }

    private void CheckSlide()
    {
        
   
        if (isSliding)
        {
            if (slideTime>=0.1)
            {
                slideTime-=Time.deltaTime;
                canMove=false;
                canFlip=false;
                rb.velocity = new Vector2(slideSpeed * facingDirection ,rb.velocity.y);
            }
            
            
            if (slideTime<=0.1)
            {
                isSliding=false;
                canMove=true;
                canFlip=true;
            }
            anim.SetBool("isSliding",isSliding);
        }
    }

    public void SlideEnd()
    {
        rb.velocity = new Vector2(0 * facingDirection ,rb.velocity.y);
    }
    

    private void CheckInput()
    {
        movementInputDirection = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump"))
        {
            if (isGrounded  || (amountOfJumpsLeft>0 && !isTouchingWall))
            {
                NormalJump();
            }
            else
            {
                jumpTimer = jumpTimerSet;
                isAttemptingToJump = true;   
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (Time.time >= slideCoolDown && Mathf.Abs(movementInputDirection)!=0 && isGrounded==true && !playerAttack.isAttacking)
            {
                slideTime = maxSlideTime;
                isSliding=true;
                
            }
                
        }


        if (Input.GetButtonDown("Horizontal") && isTouchingWall)
        {
            if (!isGrounded && movementInputDirection != facingDirection)
            {
                canMove=false;
                canFlip=false;

                turnTimer = turnTimerSet;
            }
        }

        if (turnTimer >= 0)
        {
            turnTimer-=Time.deltaTime;
            if (turnTimer<=0)
            {
                canMove=true;
                canFlip=true;
            }
        }

        if (checkJumpMultiplier && !Input.GetButton("Jump"))
        {
            checkJumpMultiplier=false;
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * variableJumpHeightMultiplier);
        }
    }

    private void CheckJump()
    {
        
       if (jumpTimer > 0)
       {
           //WallJump
           if (!isGrounded && isTouchingWall && movementInputDirection!=0 && movementInputDirection != facingDirection)
           {
               WallJump();
           }
           else if (isGrounded)
           {

               NormalJump();
           }
       }
       if(isAttemptingToJump)
       {
           jumpTimer -= Time.deltaTime; 
       }

       if (wallJumpTimer > 0)
       {
           if (hasWallJumped)
           {
               hasWallJumped = false;
           }
           else if (wallJumpTimer <= 0)
           {
               hasWallJumped = false;
           }
           else
           {
               wallJumpTimer -= Time.deltaTime;
           }
       }
        
        
    }

    private void NormalJump()
    {
        if (canNormalJump && playerAttack.attackAnimEnd)
        {
            if (grabTime)
            {
                grabTime=false;
            }
            rb.velocity = new Vector2(rb.velocity.x,jumpForce);
            amountOfJumpsLeft--;
            jumpTimer=0;
            isAttemptingToJump=false;
            checkJumpMultiplier = true;
        }
    }

    private void WallJump()
    {
        if(canWallJump)
        {
            if (movementInputDirection!=facingDirection)
            {
                rb.velocity = new Vector2(rb.velocity.x,0.0f);
                isWallSliding=false;
                amountOfJumpsLeft = amountOfJumps;
                amountOfJumpsLeft--;
                Vector2 forceToAdd = new Vector2(wallJumpForce * wallJumpDirection.x * movementInputDirection, wallJumpForce * wallJumpDirection.y);
                rb.AddForce(forceToAdd, ForceMode2D.Impulse);
                jumpTimer=0;
                isAttemptingToJump=false;
                checkJumpMultiplier = true;
                turnTimer=0;
                canMove=true;
                canFlip=true;
                hasWallJumped = true;
                wallJumpTimer = wallJumpTimerSet;
                lastWallJumpDirection = -facingDirection;
            }
            if (movementInputDirection==facingDirection)
            {
                canMove=false;
                canFlip=false;
                isWallSliding=false;
                amountOfJumpsLeft = amountOfJumps;
                amountOfJumpsLeft--;
                Vector2 forceToAdd = new Vector2((-1 * movementInputDirection) * wallJumpForce * wallJumpDirection.x, wallJumpForce * wallJumpDirection.y);
                rb.AddForce(forceToAdd, ForceMode2D.Impulse);
                jumpTimer=0;
                isAttemptingToJump=false;
                checkJumpMultiplier = true;
                turnTimer=0;
                hasWallJumped = true;
                wallJumpTimer = wallJumpTimerSet;
                turnTimer = turnTimerSet;
                lastWallJumpDirection = -facingDirection;
            }
        }
    }

    private void ApplyMovement()
    {
        if(!isGrounded && !isWallSliding && movementInputDirection==0 && !knockback)
        {
            rb.velocity = new Vector2(rb.velocity.x * airDragMultiplier, rb.velocity.y);
        }
        else if(canMove && !knockback)
        {           
            rb.velocity = new Vector2(movementSpeed * movementInputDirection, rb.velocity.y);
        }
        if (isWallSliding)
        {   
            if (rb.velocity.y < -wallSlideSpeed)
            {   
                if (!startSpawnWallEffect)
                {
                    startSpawnWallEffect = true;
                    InvokeRepeating("spawnWallSlideEffect", 0f, .5f);
                }
                rb.velocity = new Vector2(rb.velocity.x, -wallSlideSpeed);
            }
        }
    }
    
    private void spawnWallSlideEffect()
    {
        if (facingDirection==1)
        {
            Instantiate(wallSlideEffect,WallParticleSpawnPoint.position,Quaternion.Euler(0.0f,0.0f,0.0f));
        }
        else
        {
            Instantiate(wallSlideEffect,WallParticleSpawnPoint.position,Quaternion.Euler(0.0f,180.0f,0.0f));
        }
        
    }

    private void SpriteBlinkingEffect()
      {
        
        if(Time.time >= knockbackStartTime + knockbackDuration)
        {
            startBlinking = false;
             spriteBlinkingTotalTimer = 0.0f;
             m_SpriteRenderer.enabled = true;   // according to 
                      //your sprite
            canMove=true;
            m_SpriteRenderer.color = Color.white;
             return;
          }
     
     spriteBlinkingTimer += Time.deltaTime;
     if(spriteBlinkingTimer >= spriteBlinkingMiniDuration)
     {
         spriteBlinkingTimer = 0.0f;
         if (m_SpriteRenderer.enabled == true) {
             m_SpriteRenderer.enabled = false;  //make changes
         } else {
             m_SpriteRenderer.enabled = true;   //make changes
         }
     }
      }

  

    private void Flip()
    {
        if (!isWallSliding && canFlip && !knockback)
        {
            facingDirection*=-1;
            isFacingRight = !isFacingRight;
            Vector3 myScale = transform.localScale;
            myScale.x *= -1;
            transform.localScale = myScale;
            wallCheckDistance *= -1;
        }
    }    
    public bool GetSlideStatus()
    {
        return isSliding;
    }
    
    public void Knockback(int direction)
    {
        
        knockback = true;
        startBlinking = true;
        m_SpriteRenderer.color = Color.red;
        knockbackStartTime = Time.time;
        rb.velocity = new Vector2(knockbackSpeed.x * direction, knockbackSpeed.y);
        anim.SetBool("IsHurted",knockback);
        anim.SetBool("IsAttacking",false);
    }
    private void CheckKnockback()
    {
        if (Time.time >= knockbackStartTime + knockbackDuration && knockback)
        {
            knockback=false;
            startBlinking = false;
            rb.velocity = new Vector2(0.0f, rb.velocity.y);
            anim.SetBool("IsHurted",knockback);
        }
    }


    private void OnDrawGizmos() {
        Gizmos.DrawWireSphere(groundCheck.position,groundCheckRadius);
        Gizmos.DrawLine(wallCheck.position,new Vector3(wallCheck.position.x + wallCheckDistance, wallCheck.position.y, wallCheck.position.z));
        
    }

   
}
