﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaScript : MonoBehaviour
{
    protected AttackDetails attackDetails;
    public GameObject splashParticle;
    private void Start() {
        attackDetails.damageAmount = 100f;
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.CompareTag("Player"))
        {
            Instantiate(splashParticle,new Vector2(other.transform.position.x,other.transform.position.y+0.4f), Quaternion.Euler(0.0f,0.0f,0.0f));
            other.SendMessage("Damage", attackDetails);
        }
    }
}
