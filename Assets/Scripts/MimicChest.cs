﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MimicChest : MonoBehaviour
{
   [SerializeField] GameObject canvas;
   private bool isChestOpen = false;
   private Enemy5 enemy;

   private void Start() {
       enemy = GetComponentInParent<Enemy5>();
   }

   private void OnTriggerStay2D(Collider2D other) {
        if (!isChestOpen)
        {  
            if (other.CompareTag("Player"))
            {
                canvas.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    enemy.openMimicChest();
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        canvas.SetActive(false);
    }
}
