﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishSpawner : MonoBehaviour
{
    public GameObject fishPrefab;
    public Transform[] spawnpoints;
    [SerializeField]
    private float spawnTime;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating ("Spawn", 1f, spawnTime);
        InvokeRepeating ("Spawn2", 1.5f, spawnTime);

    }
    
    // Update is called once per frame

    void Spawn()
    {
        Instantiate(fishPrefab,spawnpoints[0].position,Quaternion.Euler(0.0f,0.0f,90.0f));
    }

    void Spawn2()
    {
        Instantiate(fishPrefab,spawnpoints[1].position,Quaternion.Euler(0.0f,0.0f,90.0f));
    }
}
