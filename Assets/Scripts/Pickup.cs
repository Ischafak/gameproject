﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    [System.Flags]
    public enum WeaponType {Melee, Ranged, Staff};
    public enum MeleeWeaponType {Sword, Spear};
    [HideInInspector]
    public GameAssets.WeaponRareness weaponRareness = new GameAssets.WeaponRareness();
    private Color WeaponColor;
    private Inventory inventory;
    public GameObject itemButton;
    //private GameObject ShotPoint;
    //private GameObject ShotPoint2;
    public GameObject weaponSprite;
    [HideInInspector]
    public Transform weaponPoint;
    private GameObject weapon;
    private GameObject itembuttonobject;
    private Rigidbody2D rb;
    private bool isRbGravityZero = false;
    [SerializeField]
    private GameObject rarenessLight;
    [SerializeField]
    private Sprite arrowSprite;
    [SerializeField]
    private float arrowSize;
    [SerializeField]
    private GameObject arrowPrefab;
    private PlayerAttack playerAttack;
    private int meleeFloatType;
    private Transform wpSword;
    private Transform wpSpike;

   
    public List<GameObject> actors = new List<GameObject>();
    public Vector3 shotPointPosition;

    public GameObject slot;
    int Weaponnumber;
    bool iseverythinkok=false;

    public bool avaibleTakeobject;
   
    private void Awake() {
        if (this.proficency == WeaponType.Ranged || this.proficency == WeaponType.Staff)
        {
            //ShotPoint=new GameObject();
        }
    }
    public WeaponType proficency;
    public MeleeWeaponType meleeWeaponType;
    void Start()
    {
        wpSword = GameObject.FindGameObjectWithTag("WeaponPointHolder").GetComponent<WeaponPointHolder>().weaponPointSword;
        wpSpike = GameObject.FindGameObjectWithTag("WeaponPointHolder").GetComponent<WeaponPointHolder>().weaponPointSpear;
        if (weaponRareness == GameAssets.WeaponRareness.Common)
        {
            WeaponColor = new Color32(255,255,255,215);
        }
        else if (weaponRareness == GameAssets.WeaponRareness.Rare)
        {
            WeaponColor = new Color32(23,211,255,215);
        }
        else if (weaponRareness == GameAssets.WeaponRareness.Epic)
        {
            WeaponColor = new Color32(255,23,217,215);
        }
        else
        {
            WeaponColor = new Color32(255,144,0,215);
        }
        playerAttack = GameObject.Find("Player").GetComponent<PlayerAttack>();
        if (this.proficency==WeaponType.Melee)
        {
            slot = GameObject.FindGameObjectsWithTag("Slot")[0];
            if (meleeWeaponType == MeleeWeaponType.Sword)
            {
                weaponPoint = wpSword;
            }
            if (meleeWeaponType == MeleeWeaponType.Spear)
            {
                weaponPoint = wpSpike;
            }
            
        }
        else
        {
            slot = GameObject.FindGameObjectsWithTag("Slot")[1];
            weaponPoint = GameObject.FindGameObjectWithTag("WeaponPointHolder").GetComponent<WeaponPointHolder>().bowPoint;
        }
        
        rarenessLight.GetComponent<SpriteRenderer>().color = WeaponColor;
        rb = GetComponent<Rigidbody2D>();
        
        avaibleTakeobject=false;
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        iseverythinkok=true;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.R) && avaibleTakeobject)
        {   
            FindObjectwithTag("Ranged");
            if (actors.Count>0)
            {
                if (actors[0].GetComponent<SpriteRenderer>().enabled && (this.proficency == WeaponType.Ranged || this.proficency == WeaponType.Staff))
                {   
                    if (iseverythinkok)
                    {
                        playerAttack.arrowPrefab = arrowPrefab;
                        weaponPoint.GetComponentsInChildren<Transform>()[1].localScale = new Vector3(arrowSize,arrowSize,arrowSize);
                        weaponPoint.GetComponentsInChildren<SpriteRenderer>()[0].sprite = arrowSprite;
                        playerAttack.TypeOfRangedWeapon(1);
                        iseverythinkok=false;
                        everthinkok();
                    }
                    
                }
            }
            
            FindObjectwithTag("Staff");
            if (actors.Count>0)
            {
                if (actors[0].GetComponent<SpriteRenderer>().enabled && (this.proficency == WeaponType.Ranged || this.proficency == WeaponType.Staff))
                {
                    if (iseverythinkok)
                    {
                    iseverythinkok=false;
                    everthinkok();
                    }
                }
            }

            FindObjectwithTag2("Melee",wpSword);
            if (actors.Count<=0)
            {
                 FindObjectwithTag2("Melee",wpSpike);
            }
            if (actors.Count>0)
            {
                if (actors[0].GetComponent<SpriteRenderer>().enabled && this.proficency == WeaponType.Melee)
                {
                    if (iseverythinkok)
                    {
                        if (meleeWeaponType == MeleeWeaponType.Sword)
                        {
                            meleeFloatType = 1;
                        }
                        else if (meleeWeaponType == MeleeWeaponType.Spear)
                        {
                            meleeFloatType = 2;
                        }
                    playerAttack.TypeOfMeleeWeapon(meleeFloatType);
                    iseverythinkok=false;
                    everthinkok();
                    }
                }
            }
        }
    }

    private void everthinkok()
    {
                if (this.proficency==WeaponType.Ranged)
                {   
                    slot.GetComponent<Slots>().DropItem(1);
                    FindObjectwithTag("Ranged");
                    if (actors.Count<=0)
                    {
                        FindObjectwithTag("Staff");
                    }
                    
                    GameObject.Destroy(actors[0]);
               }
               else if(this.proficency==WeaponType.Staff)
               {
                    slot.GetComponent<Slots>().DropItem(1);
                    FindObjectwithTag("Ranged");
                    if (actors.Count<=0)
                    {
                        FindObjectwithTag("Staff");
                    }
                    
                    GameObject.Destroy(actors[0]);
               }
               else
               {
                    slot.GetComponent<Slots>().DropItem(0);
                    FindObjectwithTag2("Melee", wpSword);
                    if (actors.Count<=0)
                    {
                        FindObjectwithTag2("Melee", wpSpike);
                    }
                    GameObject.Destroy(actors[0]);
               }

                    itembuttonobject = Instantiate(itemButton,inventory.slots[Weaponnumber].transform,false);
                    weapon = Instantiate(weaponSprite,weaponPoint);
                    weapon.tag=proficency.ToString();
                  
                    if (this.proficency == WeaponType.Ranged || this.proficency == WeaponType.Staff)
                    {
                        //ShotPoint2 = Instantiate(ShotPoint,weapon.transform);
                        //ShotPoint2.name = "Bu olsutu";
                        //ShotPoint2.transform.localPosition = shotPointPosition;
                        //weapon.GetComponent<Weapon>().shotPoint = ShotPoint2.transform;
                    }
                    GameObject.Destroy(this.gameObject);
                    
    }

    private void OnTriggerStay2D(Collider2D other) {
       
        if (other.CompareTag("Player"))
        {
            
           if (this.proficency == WeaponType.Ranged || this.proficency == WeaponType.Staff)
            {
                Weaponnumber=1;
            }
            else
            {
                Weaponnumber=0;
            }
            
                if (inventory.isFull[Weaponnumber]==false)
                {
                    
                    inventory.isFull[Weaponnumber]=true;
                    itembuttonobject = Instantiate(itemButton,inventory.slots[Weaponnumber].transform,false);

                    weapon = Instantiate(weaponSprite,weaponPoint);
                    weapon.tag=proficency.ToString();
                    if (this.proficency == WeaponType.Ranged || this.proficency == WeaponType.Staff)
                    {
                        playerAttack.arrowPrefab = arrowPrefab;
                        weaponPoint.GetComponentsInChildren<SpriteRenderer>()[0].sprite = arrowSprite;
                        weaponPoint.GetComponentsInChildren<Transform>()[1].localScale = new Vector3(arrowSize,arrowSize,arrowSize);
                        playerAttack.TypeOfRangedWeapon(1);
                        //ShotPoint2 = Instantiate(ShotPoint,weapon.transform);
                        //ShotPoint2.name = "Bu olsutu";
                        //ShotPoint2.transform.localPosition = shotPointPosition;
                    } 
                    if (this.proficency == WeaponType.Ranged || this.proficency == WeaponType.Staff)
                    {
                        //weapon.GetComponent<Weapon>().shotPoint = ShotPoint2.transform;
                    }
                    if (this.proficency == WeaponType.Melee)
                    {
                        if (meleeWeaponType == MeleeWeaponType.Sword)
                        {
                            meleeFloatType = 1;
                        }
                        else if (meleeWeaponType == MeleeWeaponType.Spear)
                        {
                            meleeFloatType = 2;
                        }
                        playerAttack.TypeOfMeleeWeapon(meleeFloatType);
                    }
                    
                    Destroy(gameObject);
                }
                else
                {
                    avaibleTakeobject=true;
                }       
        }
        
    }

    private void OnTriggerExit2D(Collider2D other) {
         avaibleTakeobject=false;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (!isRbGravityZero)
        {
            isRbGravityZero=true;
        }
    }




    public void FindObjectwithTag2(string _tag, Transform parent)
     {
         actors.Clear();
         GetChildObject(parent, _tag);
     }
     public void FindObjectwithTag(string _tag)
     {
         actors.Clear();
         Transform parent = weaponPoint.transform;
         GetChildObject(parent, _tag);
     }

    public void GetChildObject(Transform parent, string _tag)
     {
         for (int i = 0; i < parent.childCount; i++)
         {
             Transform child = parent.GetChild(i);
             if (child.tag == _tag)
             {
                 actors.Add(child.gameObject);
             }
         }
     }
}
