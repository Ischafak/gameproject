﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishPhysics : MonoBehaviour
{
    [SerializeField]
    private float fishSpeedX,fishSpeedY,fishTorque,fishDamage;

    [SerializeField]
    private GameObject splashParticle;
    private Rigidbody2D rb;
    [SerializeField]
    private float lifeTime = 2;
    private FishSpawner fishSpawner;
    private float yVelocity;
    public float attackDamage = 10.0f;
    private PlayerControllerX player;
    protected AttackDetails attackDetails;
    
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyProjectile",lifeTime);
        rb= GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(fishSpeedX , fishSpeedY);
        rb.AddTorque(-fishTorque , ForceMode2D.Impulse);
        attackDetails.damageAmount = attackDamage;
        attackDetails.position = this.transform.position;
    }

    private void Update() {
        yVelocity = rb.velocity.y;
    }
    // Update is called once per frame
    private void DestroyProjectile()
    {
        Destroy(gameObject);
    }


    private void OnTriggerEnter2D(Collider2D other) {
        if (other.transform.tag=="WaterTopSide" && yVelocity<0 )
        {
            Instantiate(splashParticle,this.transform.position, Quaternion.Euler(0.0f,0.0f,0.0f));
        }
        if (other.CompareTag("Player"))
        {
            other.SendMessage("Damage", attackDetails);
        }
    }
}
