﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    [SerializeField]
    private float MaxHealth = 100f;

    [SerializeField]
    private GameObject deathChunkParticle, deathBloodParticle;

    private float currentHealth;
    private GameManager GM;

    public HealthBar healthBar;

    private void Start() {
        currentHealth = MaxHealth;
        healthBar.SetMaxHealth(MaxHealth);
        GM = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    public void DecreaseHealth(float amount)
    {
        currentHealth -= amount;
        healthBar.SetHealth(currentHealth);
        if (currentHealth <= 0.0f)
        {
            Die();
        }
    }

    private void Die() 
    {
        
        //Instantiate(deathChunkParticle, transform.position, deathChunkParticle.transfrom.rotation);
        //bloodparticle yukarıdaki gibi
        Destroy(gameObject);
        
    }
}
