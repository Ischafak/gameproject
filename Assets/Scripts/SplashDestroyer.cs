﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashDestroyer : MonoBehaviour
{
   public void GameObjectDestroy()
   {
       Destroy(gameObject);
   }
}
