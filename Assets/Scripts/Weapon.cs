﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform shotPoint;
    private float timeBtwShots;
    public float startTimeBtwShots;
    public bool drag=false;
    public GameObject projectile;
    public GameObject blastEffect;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0)) 
        {
            drag=false;
        }
        else if (Input.GetMouseButtonDown(0))
        {
            drag=true;
        }
       

    if (timeBtwShots<=0)
    {
        if (drag==true)
        {
            Instantiate(projectile, shotPoint.position,transform.rotation);
            projectile.GetComponent<Projectile>().whoshot = "Player";
            Instantiate(blastEffect, shotPoint.position,transform.rotation);
           
            timeBtwShots = startTimeBtwShots;  
        }
    }
    else
    {
            timeBtwShots -=Time.deltaTime;
    }

       
    }
    
}
