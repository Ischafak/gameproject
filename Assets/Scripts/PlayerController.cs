﻿using UnityEngine;
using System.Collections;
using Cinemachine;


public class PlayerController : MonoBehaviour
{

    private CinemachineVirtualCamera cinemac;
    public float maxSpeed = 6f;
    public float jumpForce = 1000f;
    public Transform groundCheck;
    public LayerMask whatIsGround;
    public float verticalSpeed = 20;
    [HideInInspector]
    public bool lookingRight = true;
    public Transform ledgeCheck;
    public Transform wallCheck;
    public float wallCheckDistance;

    public float ledgeClimbXOffset1 = 0f;
    public float ledgeClimbYOffset1 = 0f;
    public float ledgeClimbXOffset2 = 0f;
    public float ledgeClimbYOffset2 = 0f;

    private GameObject Camera;
    private Animator camAnim;
    bool doubleJump = false;

    private bool spawnDust;

    float camRayLenght = 100f;

    Rigidbody2D playerRigidbody;

    //public GameObject dustEffect;
    //public ParticleSystem trailEffect;
    private bool trail1time;
    private bool isTouchingLedge;
    private bool canClimbLedge = false;
    private bool ledgeDetected;
    private bool isTouchingWall;
    private Rigidbody2D rb2d;
    private Animator anim;
    private bool isGrounded = false;
    private float timeBtwTrail;
    private GameObject[] swordColliders;
    private float startTimeBtwTrail = 0.5f;
    private Vector2 ledgePosBot;
    private Vector2 ledgepos1;
    private Vector2 ledgepos2;




    // Use this for initialization

    void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
    }
    void Start()
    {
        Camera = GameObject.FindGameObjectWithTag("Camera");
        camAnim = Camera.GetComponent<Animator>();
        //trailEffect.Stop();
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        //cloudanim = GetComponent<Animator>();
        //cloudanim = GameObject.Find("Cloud(Clone)").GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckLedgeClimb();
          

        if (Input.GetButtonDown("Jump") && (isGrounded || !doubleJump))
        {
            anim.SetTrigger("takeOf");
            playerRigidbody.velocity = Vector2.up * jumpForce;

            if (!doubleJump && !isGrounded)
            {
                doubleJump = true;
                //	cloudanim.Play("cloud");		
            }
        }

        isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.15F, whatIsGround);

        if (isGrounded)
        {
            if (spawnDust == true)
            {
                //camAnim.SetTrigger("Shake");
                //Instantiate(dustEffect,groundCheck.position, Quaternion.identity);
                spawnDust = false;
            }
            doubleJump = false;
            anim.SetBool("isJumping", false);
        }
        else
        {
            spawnDust = true;
            anim.SetBool("isJumping", true);
        }

    }

    void FixedUpdate()
    {
        CheckSurroundings(); 
        float hor = Input.GetAxis("Horizontal");
        anim.SetFloat("Speed", Mathf.Abs(hor));

        /*if (Mathf.Abs(hor)!=0)
		{
			if (!trailEffect.isPlaying && isGrounded)
			{
				trailEffect.Play();
			}
			if (timeBtwTrail<=0 && isGrounded)
			{
				
				timeBtwTrail=startTimeBtwTrail;
			}
			else
			{
				timeBtwTrail-=Time.deltaTime;
			}
		}
		else
		{
			trailEffect.Stop();
		}
		*/

        rb2d.velocity = new Vector2(hor * maxSpeed, rb2d.velocity.y);

        if ((hor > 0 && !lookingRight) || (hor < 0 && lookingRight))
        {
            Flip();
        }

    }
     private void CheckLedgeClimb()
     {
         if (ledgeDetected && !canClimbLedge)
         {
             canClimbLedge=true;

             if (lookingRight)
             {
                 ledgepos1 = new Vector2(Mathf.Floor(ledgePosBot.x+wallCheckDistance)-ledgeClimbXOffset1,Mathf.Floor(ledgePosBot.y) + ledgeClimbYOffset1);
                 ledgepos2 = new Vector2(Mathf.Floor(ledgePosBot.x+wallCheckDistance)+ledgeClimbXOffset2,Mathf.Floor(ledgePosBot.y) + ledgeClimbYOffset2);
             }
             else
             {
                 ledgepos1 = new Vector2(Mathf.Ceil(ledgePosBot.x-wallCheckDistance)+ledgeClimbXOffset1,Mathf.Floor(ledgePosBot.y) + ledgeClimbYOffset1);
                 ledgepos2 = new Vector2(Mathf.Ceil(ledgePosBot.x+wallCheckDistance)-ledgeClimbXOffset2,Mathf.Floor(ledgePosBot.y) + ledgeClimbYOffset2);
             }
             maxSpeed=0f;
            
           anim.SetBool("canClimbLedge",canClimbLedge);
         }
     }

    public void FinishLedgeClimb()
    {
        canClimbLedge=false;
        transform.position=ledgepos2;
        maxSpeed=6f;
        ledgeDetected=false;
        anim.SetBool("canClimbLedge",canClimbLedge);
    }

    private void CheckSurroundings()
    {
        isTouchingWall = Physics2D.Raycast(wallCheck.position,transform.right,wallCheckDistance,whatIsGround);
        isTouchingLedge = Physics2D.Raycast(ledgeCheck.position, transform.right,wallCheckDistance,whatIsGround);

        if (isTouchingWall && !isTouchingLedge && !ledgeDetected)
        {
            ledgeDetected=true;
            ledgePosBot = wallCheck.position;
        }
    }

    

    public void BeginAttack()
    {
        swordColliders = GameObject.FindGameObjectsWithTag("Melee");
        foreach (var weapon in swordColliders)
        {
            weapon.GetComponent<BoxCollider2D>().enabled = true;

        }
    }

    public void EndAttack()
    {
        swordColliders = GameObject.FindGameObjectsWithTag("Melee");
        foreach (var weapon in swordColliders)
        {
            weapon.GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    public void Flip()
    {
        lookingRight = !lookingRight;
        Vector3 myScale = transform.localScale;
        myScale.x *= -1;
        transform.localScale = myScale;
    }




}
