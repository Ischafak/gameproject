﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
  public GameObject item;
  private Transform player;
  private GameObject[] slot;

  private Transform Rotat;

  private GameObject itemobject;
  private Transform wepoint;
  private float playerPositionYOffSet = 0.3f;
  
  private void Start() {
      player = GameObject.FindGameObjectWithTag("Player").transform;
      slot = GameObject.FindGameObjectsWithTag("Slot");
  }
  public void SpawnDroppedItem(int weaponNumber)
  {
     
      Vector2 playerPos = new Vector2(player.position.x, player.position.y + playerPositionYOffSet);
      itemobject = Instantiate(item,playerPos, Quaternion.Euler (0f, 0f, 0f));
     
      itemobject.GetComponent<Pickup>().slot = slot[weaponNumber];
      
      
  }
}
