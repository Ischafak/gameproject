using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowScript : MonoBehaviour
{

    [SerializeField]
    private float arrowSpeedX,arrowSpeedY,arrowTorque,arrowDamage;
    [SerializeField]
    private float lifeTime;
    [SerializeField]
    private GameObject hitParticle;
    private Rigidbody2D rb;
    [SerializeField]
    private float damageRadius;
    [SerializeField]
    private LayerMask whatIsSolid;
    private int playerFacingDirection;
    private PlayerControllerX playerController;
    private bool isHitted = false;
    private AttackDetails attackDetails;
    [SerializeField]
    private float stunDamage;
    [SerializeField]
    private float zOffset;
    private enum ArrowEffect {Normal, Freeze, Poison};
    [SerializeField]
    private ArrowEffect arrowEffect;
    

    void Start()
    {
        float zRotation = this.transform.rotation.z + zOffset;
        this.transform.Rotate(0f, 0f, zRotation);
        playerController = GameObject.Find("Player").GetComponent<PlayerControllerX>();
        rb= GetComponent<Rigidbody2D>();
        Invoke("DestroyProjectile",lifeTime);
        playerFacingDirection = playerController.GetFacingDirection();
        rb.velocity = new Vector2(arrowSpeedX * playerFacingDirection, arrowSpeedY);
        rb.AddTorque(arrowTorque * -playerFacingDirection, ForceMode2D.Impulse);
        attackDetails.damageAmount = arrowDamage;
        attackDetails.position = transform.position;
        attackDetails.stunDamageAmount = stunDamage;
        if (arrowEffect == ArrowEffect.Freeze)
        {
            attackDetails.freeze = true;
        }
        

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!isHitted)
        {
            
            Collider2D groundHit = Physics2D.OverlapCircle(transform.position, damageRadius, whatIsSolid);
            if (groundHit != null)
            {
                //if (hitInfo.collider.CompareTag("Enemy"))
                //{
                    
                //}
                isHitted=true;
                Instantiate(hitParticle,this.transform.position, Quaternion.Euler(0.0f,0.0f, Random.Range(0.0f, 360.0f)));
                
                if (groundHit.tag=="Enemy")
                {
                    groundHit.transform.parent.SendMessage("Damage",attackDetails);
                    Destroy(gameObject);
                }
                else
                {
                    DestroyProjectile();
                }
                
            }
        }
    }

    private void DestroyProjectile()
    {
        rb.angularVelocity = 0.0f;
        rb.isKinematic = true;
        rb.velocity = new Vector2(0.0f, 0.0f);
        //Destroy(gameObject);
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireSphere(this.transform.position, damageRadius); 
    }
}
